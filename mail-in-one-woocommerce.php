<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://treaction.de
 * @since             1.0.0
 * @package           MailInOne_Woocommerce
 *
 * @wordpress-plugin
 * Plugin Name:       Mail-In-One for WordPress
 * Plugin URI:        http://treaction.net/plugins/en/wordpress
 * Description:       Mail-In-One for WordPress
 * Version:           1.2.0
 * Author:            treaction ag
 * Author URI:        http://treaction.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mail-in-one-woocommerce
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

// Add a new interval of 15 minutes
add_filter( 'cron_schedules', 'mail_in_one_add_cron_schedule' );

// Schedule an action if it's not already scheduled
if ( ! wp_next_scheduled( 'mail_in_one_contacts_cron_action' ) ) {
    wp_schedule_event( time(), 'every-15-minutes', 'mail_in_one_contacts_cron_action' );
}

function mail_in_one_add_cron_schedule( $schedules ) {
    $schedules[ 'every-15-minutes' ] = array(
                 'interval' => 300,
                 'display' =>  'Every 15 minutes');
    return $schedules;
}


// Hook into that action that'll fire every 15 minutes
add_action( 'mail_in_one_contacts_cron_action', 'cron_mail_in_one_contacts' );

function cron_mail_in_one_contacts(){
    
    $plugin_dir_path = plugin_dir_path( __FILE__ );
    require_once( $plugin_dir_path . 'cron/mail-in-one-woocommerce-contacts.php' );   
}

/**
 * @return object
 */
function mail_in_one_environment_variables() {
    global $wp_version;
    
    $o = get_option('mail-in-one-woocommerce', false);
    
    return (object) array(
        'repo' => 'master',
        'environment' => 'production',
        'version' => '1.2.0',
        'wp_version' => (empty($wp_version) ? 'Unknown' : $wp_version),
        'wc_version' => class_exists('WC') ? WC()->version : null,
        'logging' => ($o && is_array($o) && isset($o['mail_in_one_logging'])) ? $o['mail_in_one_logging'] : 'none',
    );
}

/**
 * @return bool|MailInOne_Woocommerce_MailInOneApi
 */
function mail_in_one_get_api() {
    if (($options = get_option('mail-in-one-woocommerce', false)) && is_array($options)) {
        if (isset($options['mail_in_one_api_key'])) {
            return new MailInOne_Woocommerce_MailInOneApi($options['mail_in_one_api_key']);
        }
    }
    return false;
}
/**
 * @param $key
 * @param null $default
 * @return null
 */
function mail_in_one_get_option($key, $default = null) {
    $options = get_option('mail-in-one-woocommerce');
    if (!is_array($options)) {
        return $default;
    }
    if (!array_key_exists($key, $options)) {
        return $default;
    }
    return $options[$key];
}

/**
 * @param $key
 * @param null $default
 * @return mixed
 */
function mail_in_one_get_data($key, $default = null) {
    return get_option('mail-in-one-woocommerce-'.$key, $default);
}

/**
 * @param $key
 * @param $value
 * @param string $autoload
 * @return bool
 */
function mail_in_one_set_data($key, $value, $autoload = 'yes') {
    return update_option('mail-in-one-woocommerce-'.$key, $value, $autoload);
}

/**
 * @param $date
 * @return DateTime
 */
function mail_in_one_date_utc($date) {
    $timezone = wc_timezone_string();
    if (is_numeric($date)) {
        $stamp = $date;
        $date = new \DateTime('now', new DateTimeZone($timezone));
        $date->setTimestamp($stamp);
    } else {
        $date = new \DateTime($date, new DateTimeZone($timezone));
    }
    
    $date->setTimezone(new DateTimeZone('UTC'));
    return $date;
}

/**
 * @param $date
 * @return DateTime
 */
function mail_in_one_date_local($date) {
    #$timezone = mail_in_one_get_option('store_timezone', 'America/New_York');
    $timezone = 'Europe/Berlin';
    if (is_numeric($date)) {
        $stamp = $date;
        $date = new \DateTime('now', new DateTimeZone('UTC'));
        $date->setTimestamp($stamp);
    } else {
        $date = new \DateTime($date, new DateTimeZone('UTC'));
    }
    
    $date->setTimezone(new DateTimeZone($timezone));
    return $date;
}

/**
 * @param array $data
 * @return mixed
 */
function mail_in_one_array_remove_empty($data) {
    if (empty($data) || !is_array($data)) {
        return array();
    }
    foreach ($data as $key => $value) {
        if ($value === null || $value === '') {
            unset($data[$key]);
        }
    }
    return $data;
}

/**
 * @return array
 */
function mail_in_one_get_timezone_list() {
    $zones_array = array();
    $timestamp = time();
    $current = date_default_timezone_get();
    
    foreach(timezone_identifiers_list() as $key => $zone) {
        date_default_timezone_set($zone);
        $zones_array[$key]['zone'] = $zone;
        $zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
    }
    
    date_default_timezone_set($current);
    
    return $zones_array;
}

/**
 * Determine if a given string contains a given substring.
 *
 * @param  string  $haystack
 * @param  string|array  $needles
 * @return bool
 */
function mail_in_one_string_contains($haystack, $needles) {
    foreach ((array) $needles as $needle) {
        if ($needle != '' && mb_strpos($haystack, $needle) !== false) {
            return true;
        }
    }
    
    return false;
}



#define( 'mail_in_one_woocommerce_VERSION', '1.2.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-mail-in-one-woocommerce-activator.php
 */
function activate_mail_in_one_woocommerce() {
    // if we don't have woocommerce we need to display a horrible error message before the plugin is installed.
    if (!is_plugin_active('woocommerce/woocommerce.php')) {
        
        $active = false;
        
        // some people may have uploaded a specific version of woo, so we need a fallback checker here.
        foreach (array_keys(get_plugins()) as $plugin) {
            if (mail_in_one_string_contains($plugin, 'woocommerce.php')) {
                $active = true;
                break;
            }
        }
        
        if (!$active) {
            // Deactivate the plugin
            deactivate_plugins(__FILE__);
            $error_message = __('The Mail In One For WooCommerce plugin requires the <a href="http://wordpress.org/extend/plugins/woocommerce/">WooCommerce</a> plugin to be active!', 'woocommerce');
            wp_die($error_message);
        }
    }
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-mail-in-one-woocommerce-activator.php';
    mailInOne_Woocommerce_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-mail-in-one-woocommerce-deactivator.php
 */
function deactivate_mail_in_one_woocommerce() {
    
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-mail-in-one-woocommerce-deactivator.php';
    mailInOne_Woocommerce_Deactivator::deactivate();
}


/**
 * @param $action
 * @param $message
 * @param null $data
 */
function mail_in_one_debug($action, $message, $data = null) {
    if (mail_in_one_environment_variables()->logging === 'debug') {
        if (is_array($data) && !empty($data)) $message .= " :: ".wc_print_r($data, true);
        wc_get_logger()->debug("{$action} :: {$message}", array('source' => 'mail_in_one_woocommerce'));
    }
}

/**
 * @param $action
 * @param $message
 * @param array $data
 * @return array|WP_Error
 */
function mail_in_one_log($action, $message, $data = array()) {
    if (mail_in_one_environment_variables()->logging !== 'none') {
        if (is_array($data) && !empty($data)) $message .= " :: ".wc_print_r($data, true);
        wc_get_logger()->notice("{$action} :: {$message}", array('source' => 'mail_in_one_woocommerce'));
    }
}

/**
 * @param $action
 * @param $message
 * @param array $data
 * @return array|WP_Error
 */
function mail_in_one_error($action, $message, $data = array()) {
    if (mail_in_one_environment_variables()->logging !== 'none') {
        if ($message instanceof \Exception) $message = mail_in_one_error_trace($message);
        if (is_array($data) && !empty($data)) $message .= " :: ".wc_print_r($data, true);
        wc_get_logger()->error("{$action} :: {$message}", array('source' => 'mail_in_one_woocommerce'));
    }
}

/**
 * @param Exception $e
 * @param string $wrap
 * @return string
 */
function mail_in_one_error_trace(\Exception $e, $wrap = "") {
    $error = "{$e->getMessage()} on {$e->getLine()} in {$e->getFile()}";
    if (empty($wrap)) return $error;
    return "{$wrap} :: {$error}";
}

register_activation_hook( __FILE__, 'activate_mail_in_one_woocommerce' );
register_deactivation_hook( __FILE__, 'deactivate_mail_in_one_woocommerce' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-mail-in-one-woocommerce.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_mail_in_one_woocommerce() {
    $env = mail_in_one_environment_variables();  
    $plugin = new MailInOne_Woocommerce($env->environment, $env->version);
    $plugin->run();
    
}
run_mail_in_one_woocommerce();
