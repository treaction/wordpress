<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    MailInOne_Woocommerce
 * @subpackage MailInOne_Woocommerce/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    MailInOne_Woocommerce
 * @subpackage MailInOne_Woocommerce/admin
 * @author     Sami Jarmoud <sami.jarmoud@treaction.de>
 */
class MailInOne_Woocommerce_Admin extends MailInOne_Woocommerce_Options {

    /**
     * @return MailInOne_Woocommerce_Admin
     */
    public static function connect() {
        $env = mail_in_one_environment_variables();

        return new self('mail-in-one-woocommerce', $env->version);
    }

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Plugin_Name_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Plugin_Name_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/mail-in-one-woocommerce-admin.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Plugin_Name_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Plugin_Name_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/mail-in-one-woocommerce-admin.js', array('jquery'), $this->version, false);
    }

    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     *
     * @since    1.0.0
     */
    public function add_plugin_admin_menu() {
        /*
         *  Documentation : http://codex.wordpress.org/Administration_Menus
         */
        #add_options_page('Mail In One - WooCommerce Setup', 'Mail In One', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page'));
        add_menu_page('Mail In One Conatct Form', 'Mail In One for WP', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page'), plugins_url('/images/mio-wp-icon.png', __FILE__));
        $menu_items = array(
            array(
                'title' => 'General',
                'text' => 'General',
                'slug' => 'mail-in-one-woocommerce&tab=api_key',
                'callback' => array($this, 'show_generals_setting_page')
            ),
            array(
                'title' => 'Get API Key',
                'text' => 'Get API Key',
                'slug' => 'mail-in-one-woocommerce&tab=store_info',
                'callback' => array($this, 'show_settings_page')
            ),
            array(
                'title' => 'Email Notification',
                'text' => 'Email Notification',
                'slug' => 'mail-in-one-woocommerce&tab=email_notification',
                'callback' => array($this, 'show_email_notification_page')
            ),
            array(
                'title' => 'Synchronize All',
                'text' => 'Synchronize All',
                'slug' => 'mail-in-one-woocommerce&tab=sync',
                'callback' => array($this, 'show_sync_page')
            ),
            array(
                'title' => 'Form Build',
                'text' => 'Form Build',
                'slug' => 'mail-in-one-woocommerce&tab=newsletter',
                'callback' => array($this, 'show_form_build_page')
            ),
            array(
                'title' => 'Content',
                'text' => 'Content',
                'slug' => 'mail-in-one-woocommerce&tab=messages',
                'callback' => array($this, 'show_messages_page')
            ),
            array(
                'title' => 'Styling',
                'text' => 'Styling',
                'slug' => 'mail-in-one-woocommerce&tab=styling',
                'callback' => array($this, 'show_styling_page')
            ),
        );

        // add sub-menu items
        foreach ($menu_items as $item) {
            $this->add_menu_item($item);
        }
    }

    /**
     * @param array $item
     */
    public function add_menu_item(array $item) {

        // register page
        add_submenu_page($this->plugin_name, $item['title'], $item['text'], 'manage_options', $item['slug'], $item['callback']);

        // register callback for loading this page, if given
        /* if( array_key_exists( 'load_callback', $item ) ) {
          add_action( 'load-' . $hook, $item['load_callback'] );
          } */
    }

    function show_form_build_page() {
        include_once 'tabs/newsletter.php';
    }

    function show_generals_setting_page() {
        include_once 'tabs/api_key.php';
    }

    function show_settings_page() {
        include_once 'tabs/store_info.php';
    }

    function show_email_notification_page() {
        include_once 'tabs/email_notification.php';
    }

    function show_sync_page() {
        include_once 'tabs/store_sync.php';
    }

    function show_messages_page() {
        include_once 'tabs/messages.php';
    }

    function show_styling_page() {
        include_once 'tabs/styling.php';
    }

    /**
     * Add settings action link to the plugins page.
     *
     * @since    1.0.0
     */
    public function add_action_links($links) {
        /*
         *  Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
         */
        $settings_link = array(
            '<a href="' . admin_url('options-general.php?page=' . $this->plugin_name) . '">' . __('Settings', $this->plugin_name) . '</a>',
        );

        return array_merge($settings_link, $links);
    }

    /**
     * Admin bar
     *
     * @param WP_Admin_Bar $wp_admin_bar
     */
    public function admin_bar($wp_admin_bar) {
        if (!current_user_can('manage_options')) {
            return;
        }
        $wp_admin_bar->add_menu(array(
            'id' => 'mail-in-one-woocommerce',
            'title' => __('MailInOne', 'mail-in-one-woocommerce'),
            'href' => '#',
        ));
        $wp_admin_bar->add_menu(array(
            'parent' => 'mail-in-one-woocommerce',
            'id' => 'mail-in-one-woocommerce-api-key',
            'title' => __('API Key', 'mail-in-one-woocommerce'),
            'href' => wp_nonce_url(admin_url('options-general.php?page=mail-in-one-woocommerce&tab=api_key'), 'mc-api-key'),
        ));
        $wp_admin_bar->add_menu(array(
            'parent' => 'mail-in-one-woocommerce',
            'id' => 'mail-in-one-woocommerce-store-info',
            'title' => __('Store Info', 'mail-in-one-woocommerce'),
            'href' => wp_nonce_url(admin_url('options-general.php?page=mail-in-one-woocommerce&tab=store_info'), 'mc-store-info'),
        ));
        $wp_admin_bar->add_menu(array(
            'parent' => 'mail-in-one-woocommerce',
            'id' => 'mail-in-one-woocommerce-email-notification',
            'title' => __('Email Notification', 'mail-in-one-woocommerce'),
            'href' => wp_nonce_url(admin_url('options-general.php?page=mail-in-one-woocommerce&tab=email_notification'), 'mc-email-notification'),
        ));
        $wp_admin_bar->add_menu(array(
            'parent' => 'mail-in-one-woocommerce',
            'id' => 'mail-in-one-woocommerce-campaign-defaults',
            'title' => __('Campaign Defaults', 'mail-in-one-woocommerce'),
            'href' => wp_nonce_url(admin_url('options-general.php?page=mail-in-one-woocommerce&tab=campaign_defaults'), 'mc-campaign-defaults'),
        ));
        $wp_admin_bar->add_menu(array(
            'parent' => 'mail-in-one-woocommerce',
            'id' => 'mail-in-one-woocommerce-newsletter-settings',
            'title' => __('Newsletter Settings', 'mail-in-one-woocommerce'),
            'href' => wp_nonce_url(admin_url('options-general.php?page=mail-in-one-woocommerce&tab=newsletter_settings'), 'mc-newsletter-settings'),
        ));

        // only display this button if the data is not syncing and we have a valid api key
        if ((bool) $this->getOption('mail_in_one_list', false) && (bool) $this->getData('sync.syncing', false) === false) {
            $wp_admin_bar->add_menu(array(
                'parent' => 'mail-in-one-woocommerce',
                'id' => 'mail-in-one-woocommerce-sync',
                'title' => __('Sync', 'mail-in-one-woocommerce'),
                'href' => wp_nonce_url(admin_url('?mail-in-one-woocommerce[action]=sync&mail-in-one-woocommerce[action]=sync'), 'mc-sync'),
            ));
        }
    }

    /**
     * Render the settings page for this plugin.
     *
     * @since    1.0.0
     */
    public function display_plugin_setup_page() {
        include_once( 'partials/mail-in-one-woocommerce-admin-tabs.php' );
    }

    /**
     * 
     */
    public function options_update() {
        $this->handle_abandoned_cart_table();
        register_setting($this->plugin_name, $this->plugin_name, array($this, 'validate'));
    }

    /**
     * Depending on the version we're on we may need to run some sort of migrations.
     */
    public function update_db_check() {
        // grab the current version set in the plugin variables
        $version = mail_in_one_environment_variables()->version;

        // grab the saved version or default to 1.0.3 since that's when we first did this.
        $saved_version = get_site_option('mail_in_one_woocommerce_version', '1.0.3');

        // if the saved version is less than the current version
        if (version_compare($version, $saved_version) > 0) {
            // resave the site option so this only fires once.
            update_site_option('mail_in_one_woocommerce_version', $version);
        }
    }

    /**
     * @param null|array $data
     * @return bool
     */
    public function hasValidApiKey($data = null) {
        if (!$this->validateOptions(array('mail_in_one_api_key'), $data)) {
            return false;
        }

        if (($pinged = $this->getCached('api-ping-check', null)) === null) {
            if (($pinged = $this->api()->ping())) {
                $this->setCached('api-ping-check', true, 120);
            }
            return $pinged;
        }
        return $pinged;
    }

    public function getCampaign() {
        return $this->campaign();
    }

    public function getContactFilter() {
        return $this->contactFilter();
    }

    public function getTargetGroup() {
        return $this->targetGroup();
    }

    public function getDOIMailing() {
        return $this->DOIMailing();
    }

    public function getContactEvents() {
        return $this->contactEvents();
    }

    public function getCustomersFiled() {
        return $this->customersFiled();
    }

    public function showTable() {
        return $this->show();
    }

    public function getSendNotification() {
        return $this->show();
    }

    /**
     * @param array $required
     * @param null $options
     * @return bool
     */
    private function validateOptions(array $required, $options = null) {
        $options = is_array($options) ? $options : $this->getOptions();

        foreach ($required as $requirement) {
            if (!isset($options[$requirement]) || empty($options[$requirement])) {
                return false;
            }
        }

        return true;
    }

    /**
     * We need to do a tidy up function on the mail_in_one_carts table to
     * remove anything older than 30 days.
     *
     * Also if we don't have the configuration set, we need to create the table.
     */
    protected function handle_abandoned_cart_table() {
        global $wpdb;

        if (get_site_option('mail_in_one_woocommerce_db_mail_in_one_carts', false)) {
            // need to tidy up the mail_in_one_cart table and make sure we don't have anything older than 30 days old.
            $date = gmdate('Y-m-d H:i:s', strtotime(date("Y-m-d") . "-30 days"));
            $sql = $wpdb->prepare("DELETE FROM {$wpdb->prefix}mail_in_one_carts WHERE created_at <= %s", $date);
            $wpdb->query($sql);
        } else {

            // create the table for the first time now.
            $charset_collate = $wpdb->get_charset_collate();
            $table = "{$wpdb->prefix}mail_in_one_carts";

            $sql = "CREATE TABLE IF NOT EXISTS $table (
				id VARCHAR (255) NOT NULL,
				email VARCHAR (100) NOT NULL,
				user_id INT (11) DEFAULT NULL,
                cart text NOT NULL,
                created_at datetime NOT NULL
				) $charset_collate;";

            if (($result = $wpdb->query($sql)) > 0) {
                update_site_option('mail_in_one_woocommerce_db_mail_in_one_carts', true);
            }
        }
    }

    protected function add_row_table_doi($doi, $name, $typeOfIntegration) {
        global $wpdb;
        $doiArray = explode("__", $doi);
        $doi_key = $doiArray[0];
        $doi_name = $doiArray[1];

        $query_row = "SELECT name FROM `" . $wpdb->prefix . "mio_short_form` WHERE name = %s";
        $results_row = $wpdb->get_results($wpdb->prepare($query_row, $name));
        if (is_null($results_row[0]->name)) {
            $insert_form = "INSERT INTO `" . $wpdb->prefix . "mio_short_form`( short_code, name, doi_name, doi_key, typeOfIntegration, status) 
                                                            VALUES ( '[mio_cf_form id=', '" . $name . "', '" . $doi_name . "', '" . $doi_key . "', '" . $typeOfIntegration . "', 'Activ' )";
            $wpdb->query($insert_form);
        }
    }

    protected function add_row_table_none($name, $typeOfIntegration) {
        global $wpdb;

        $query_row = "SELECT name FROM `" . $wpdb->prefix . "mio_short_form` WHERE name = %s";
        $results_row = $wpdb->get_results($wpdb->prepare($query_row, $name));
        if (is_null($results_row[0]->name)) {
            $insert_form = "INSERT INTO `" . $wpdb->prefix . "mio_short_form`( short_code, name, typeOfIntegration, status) 
                                                            VALUES ( '[mio_cf_form id=', '" . $name . "', '" . $typeOfIntegration . "', 'Activ' )";
            $test = $wpdb->query($insert_form);
        }
    }

    protected function add_row_table_event($contactEvent, $name, $typeOfIntegration) {
        global $wpdb;
        $contactEventArray = explode("__", $contactEvent);
        $contactEvent_key = $contactEventArray[0];
        $contactEvent_name = $contactEventArray[1];

        $query_row = "SELECT name FROM `" . $wpdb->prefix . "mio_short_form` WHERE name = %s";
        $results_row = $wpdb->get_results($wpdb->prepare($query_row, $name));
        if (is_null($results_row[0]->name)) {
            $insert_form = "INSERT INTO `" . $wpdb->prefix . "mio_short_form`( short_code, name, contactEvent_name, contactEvent_key, typeOfIntegration, status) 
                                                            VALUES ( '[mio_cf_form id=', '" . $name . "', '" . $contactEvent_name . "', '" . $contactEvent_key . "', '" . $typeOfIntegration . "', 'Activ' )";
            $wpdb->query($insert_form);
        }
    }

    protected function add_row_table_xor($doi, $contactEvent, $name, $typeOfIntegration) {
        global $wpdb;
        $doiArray = explode("__", $doi);
        $doi_key = $doiArray[0];
        $doi_name = $doiArray[1];
        $contactEventArray = explode("__", $contactEvent);
        $contactEvent_key = $contactEventArray[0];
        $contactEvent_name = $contactEventArray[1];
        $query_row = "SELECT name FROM `" . $wpdb->prefix . "mio_short_form` WHERE name = %s";
        $results_row = $wpdb->get_results($wpdb->prepare($query_row, $name));
        if (is_null($results_row[0]->name)) {
            $insert_form = "INSERT INTO `" . $wpdb->prefix . "mio_short_form`( short_code, name, doi_name, doi_key, contactEvent_name, contactEvent_key, typeOfIntegration, status) 
                                                            VALUES ( '[mio_cf_form id=', '" . $name . "', '" . $doi_name . "', '" . $doi_key . "',  '" . $contactEvent_name . "', '" . $contactEvent_key . "', '" . $typeOfIntegration . "', 'Activ' )";
            $wpdb->query($insert_form);
        }
    }

    protected function add_row_table_ma($marketing_automation, $name, $typeOfIntegration) {
        global $wpdb;

        $query_row = "SELECT name FROM `" . $wpdb->prefix . "mio_short_form` WHERE name = %s";
        $results_row = $wpdb->get_results($wpdb->prepare($query_row, $name));
        if (is_null($results_row[0]->name)) {
            $insert_form = "INSERT INTO `" . $wpdb->prefix . "mio_short_form`( short_code, name, marketing_automation, typeOfIntegration, status) 
                                                            VALUES ( '[mio_cf_form id=', '" . $name . "', '" . $marketing_automation . "', '" . $typeOfIntegration . "','Activ' )";
            $wpdb->query($insert_form);
        }
    }

    protected function show() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM {$wpdb->prefix}mio_short_form");
    }

    protected function sendNotification() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM {$wpdb->prefix}mio_short_form");
    }

    protected function deleteRow($id) {
        global $wpdb;
        $table = "{$wpdb->prefix}mio_short_form";
        $wpdb->delete($table, array('id' => $id));
    }

    protected function updateRow($id, $doi, $status, $typeOfIntegration, $contactEvent, $marketing_automation) {
        global $wpdb;
        $doiArray = explode("__", $doi);
        $doi_key = $doiArray[0];
        $doi_name = $doiArray[1];
        $contactEventArray = explode("__", $contactEvent);
        $contactEvent_key = $contactEventArray[0];
        $contactEvent_name = $contactEventArray[1];
        switch ($typeOfIntegration) {
            case '5':
                $table = "{$wpdb->prefix}mio_short_form";
                $wpdb->update($table, array('doi_name' => '', 'doi_key' => '', 'status' => $status, 'typeOfIntegration' => 'NONE', 'contactEvent_name' => ' ', 'contactEvent_key' => ' ', 'marketing_automation' => ' '), array('id' => $id));
                break;
            case '1':
                if (!empty($doi)) {
                    $table = "{$wpdb->prefix}mio_short_form";
                    $wpdb->update($table, array('doi_name' => $doi_name, 'doi_key' => $doi_key, 'status' => $status, 'typeOfIntegration' => 'DOI', 'contactEvent_name' => ' ', 'contactEvent_key' => ' ', 'marketing_automation' => ' '), array('id' => $id));
                }
                break;
            case '2':
                if (!empty($contactEvent)) {
                    $table = "{$wpdb->prefix}mio_short_form";
                    $wpdb->update($table, array('doi_name' => ' ', 'doi_key' => ' ', 'status' => $status, 'typeOfIntegration' => 'Contact Event', 'contactEvent_name' => $contactEvent_name, 'contactEvent_key' => $contactEvent_key, 'marketing_automation' => ' '), array('id' => $id));
                }

                break;
            case '3':
                if (!empty($doi) && !empty($contactEvent)) {
                    $table = "{$wpdb->prefix}mio_short_form";
                    $wpdb->update($table, array('doi_name' => $doi_name, 'doi_key' => $doi_key, 'status' => $status, 'typeOfIntegration' => 'Contact Event if DOI already exists', 'contactEvent_name' => $contactEvent_name, 'contactEvent_key' => $contactEvent_key, 'marketing_automation' => ' '), array('id' => $id));
                }
                break;
            case '4':
                if (!empty($marketing_automation)) {
                    $table = "{$wpdb->prefix}mio_short_form";
                    $wpdb->update($table, array('doi_name' => $doi_name, 'doi_key' => ' ', 'status' => $status, 'typeOfIntegration' => 'Marketing Automation', 'contactEvent_name' => ' ', 'contactEvent_key' => ' ', 'marketing_automation' => $marketing_automation), array('id' => $id));
                }
                break;

            default:
                break;
        }
    }

    /**
     * @param $input
     * @return array
     */
    public function validate($input) {

        $active_tab = isset($input['mailInOne_active_tab']) ? $input['mailInOne_active_tab'] : null;
        if (empty($active_tab)) {
            return $this->getOptions();
        }

        switch ($active_tab) {

            case 'api_key':
                $data = $this->validatePostApiKey($input);
                break;

            case 'store_info':
                $data = $this->validatePostStoreInfo($input);
                break;
            case 'email_notification':
                $data = $this->validatePostEmailNotification($input);
                break;
            case 'newsletter':
                $data = $this->compileNewsletterData($input);
                break;
            case 'messages':
                $data = $this->compileMessagesData($input);
                break;
            case 'styling':
                $data = $this->compileStylingData($input);
                break;
            case 'sync':
                $data = $this->validatePostSync($input);
                break;

            case 'logs':

                if (isset($_POST['mc_action']) && in_array($_POST['mc_action'], array('view_log', 'remove_log'))) {
                    wp_redirect('options-general.php?page=mail-in-one-woocommerce&tab=logs');
                    exit();
                }

                $data = array(
                    'mail_in_one_logging' => isset($input['mail_in_one_logging']) ? $input['mail_in_one_logging'] : 'none',
                );
                break;
        }

        return (isset($data) && is_array($data)) ? array_merge($this->getOptions(), $data) : $this->getOptions();
    }

    /**
     * STEP 1.
     *
     * Handle the 'api_key' tab post.
     *
     * @param $input
     * @return array
     */
    protected function validatePostApiKey($input) {

        $data = array(
            'mail_in_one_api_key' => isset($input['mail_in_one_api_key']) ? $input['mail_in_one_api_key'] : false,
            'mail_in_one_debugging' => isset($input['mail_in_one_debugging']) ? $input['mail_in_one_debugging'] : false,
            'mail_in_one_account_info_id' => null,
            'mail_in_one_account_info_username' => null,
            'form_doi_key_mail_in_one' => isset($input['form_doi_key_mail_in_one']) ? $input['form_doi_key_mail_in_one'] : false,
            'mail_in_one_api_key' => isset($input['mail_in_one_api_key']) ? $input['mail_in_one_api_key'] : false,
            'mail_in_one_api_key' => isset($input['mail_in_one_api_key']) ? $input['mail_in_one_api_key'] : false,
        );

        $api = new MailInOne_Woocommerce_MailInOneApi($data['mail_in_one_api_key']);

        $valid = true;

        if (empty($data['mail_in_one_api_key']) || !($profile = $api->ping(true))) {
            unset($data['mail_in_one_api_key']);
            $valid = false;
        }

        // tell our reporting system whether or not we had a valid ping.
        $this->setData('validation.api.ping', $valid);

        $data['active_tab'] = $valid ? 'api_key' : 'api_key';

        if ($valid && isset($profile) && is_array($profile) && array_key_exists('account_id', $profile)) {
            $data['mail_in_one_account_info_id'] = $profile['account_id'];
            $data['mail_in_one_account_info_username'] = $profile['username'];
        }

        if ($valid) {
            // start the sync automatically if the sync is false
            if ((bool) $this->getData('sync.started_at', false) === false) {
                $res = $this->startSync();
                $this->showSyncStartedMessage();
            }
        }
        if ((isset($input['tname']) && isset($input['typeOfIntegration'])) && (!empty($input['tname']) && !empty($input['typeOfIntegration']))) {
            switch ($input['typeOfIntegration']) {
                case '1':
                    if (!empty($input['tdoi']) && isset($input['tdoi'])) {
                        $this->add_row_table_doi($input['tdoi'], $input['tname'], 'DOI');
                    }
                    break;
                case '2':

                    if (!empty($input['contactEvents']) && isset($input['contactEvents'])) {
                        $this->add_row_table_event($input['contactEvents'], $input['tname'], 'Contact Event');
                    }
                    break;
                case '3':
                    if ((!empty($input['tdoi']) && isset($input['tdoi'])) && (!empty($input['contactEvents']) && isset($input['contactEvents']))) {
                        $this->add_row_table_xor($input['tdoi'], $input['contactEvents'], $input['tname'], 'Contact Event if DOI already exists');
                    }
                    break;
                case '4':
                    if (!empty($input['marketingAutomation']) && isset($input['marketingAutomation'])) {
                        $this->add_row_table_ma((int) $input['marketingAutomation'], $input['tname'], 'Marketing Automation');
                    }
                    break;
                case '5':
                    $this->add_row_table_none($input['tname'], 'NONE');
                    break;
                default:
                    break;
            }
        }

        if (isset($input['cf_id']) && !empty($input['cf_id']) && $input['action'] == 'delete') {
            $this->deleteRow($input['cf_id']);
        }
        if (isset($input['cf_id3']) && !empty($input['cf_id3']) && $input['action3'] == 'update') {
            $this->updateRow($input['cf_id3'], $input['tdoi3'], $input['cf_status3'], $input['typeOfIntegration3'], $input['contactEvent3'], $input['marketing_automation3']);
        }

        return $data;
    }

    /**
     * STEP 2.
     *
     * Handle the 'store_info' tab post.
     *
     * @param $input
     * @return array
     */
    protected function validatePostStoreInfo($input) {

        $data = $this->compileStoreInfoData($input);

        if (!$this->hasValidStoreInfo($data)) {

            $this->setData('validation.store_info', false);
            $data['active_tab'] = 'store_info';

            return array();
        } else {

            $this->setData('validation.store_info', true);
            $to = 'campaign@treaction.de';
            $subject = 'Neue Mandant';
            $messageBody = $data['store_name'];
            $messageBody = 'Bitte ein neue Mandant anlegen'
                    . \chr(13) . \chr(13)
                    . '========================================' . \chr(13)
                    . 'Vorname: ' . $data['store_name'] . \chr(13)
                    . 'Nachname: ' . $data['store_lastname'] . \chr(13)
                    . 'Straße: ' . $data['store_street'] . \chr(13)
                    . 'PLZ: ' . $data['store_postal_code'] . \chr(13)
                    . 'Ort: ' . $data["store_city"] . \chr(13)
                    . 'Land: ' . $data["store_country"] . \chr(13)
                    . 'Email: ' . $data["admin_email"] . \chr(13)
                    . 'Telefonnummer: ' . $data['store_phone'] . \chr(13)
                    . '========================================' . \chr(13)
            ;
            wp_mail(
                    $to, $subject, $messageBody
            );

            $this->addInvalidPhoneAlert();
            return $data;
        }
    }

    /**
     *
     * Handle the 'Email_Notification' tab post.
     *
     * @param $input
     * @return array
     */
    protected function validatePostEmailNotification($input) {

        $data = $this->compileEmailNotificationData($input);
        if (!$this->hasValidEmailNotification($data)) {

            $this->setData('validation.email_notification', false);
            $data['active_tab'] = 'email_notification';

            return array();
        } else {

            $this->setData('validation.email_notification', true);
            $to = $data['notification_email_notification_list'];
            $subject = $data['notification_email_subject'];
            $messageBody = $data['notification_email_notification'];
            #wp_mail(
            #  $to, $subject, $messageBody
            #);

            return $data;
        }
    }

    /**
     * 
     * @param $input
     * @return array
     */
    protected function validatePostSync($input) {

        return $this->startSync();
    }

    /**
     * Start the sync
     */
    public function startSync() {

        $customers = new MailInOne_Woocommerce_MailInOneApi($this->getOption('mail_in_one_api_key'));

        $this->setData('sync.config.resync', false);
        $this->setData('sync.syncing', true);
        $this->setData('sync.started_at', time());

        mail_in_one_log('sync.started', "Starting Sync :: " . date('D, M j, Y g:i A'));
        $result = $customers->sync();
        $statusCode = $result->getStatusCode();
        if ($statusCode == '201') {
            // this is the last thing we're doing so it's complete as of now.
            $this->setData('sync.syncing', false);
            $this->setData('sync.completed_at', time());

            mail_in_one_log('sync.completed', "Finished Sync :: " . date('D, M j, Y g:i A'));
        }
        return $result;
    }

    /**
     * Start the sync
     */
    public function cronStartSync() {
        $customers = new MailInOne_Woocommerce_MailInOneApi($this->getOption('mail_in_one_api_key'));

        $this->setData('sync.config.resync', false);
        $this->setData('sync.syncing', true);
        $this->setData('sync.started_at', time());

        mail_in_one_log('sync.started', "Starting Sync :: " . date('D, M j, Y g:i A'));
        $result = $customers->cronSync();
        $statusCode = $result->getStatusCode();
        if ($statusCode == '201') {
            // this is the last thing we're doing so it's complete as of now.
            $this->setData('sync.syncing', false);
            $this->setData('sync.completed_at', time());

            mail_in_one_log('sync.completed', "Finished Sync :: " . date('D, M j, Y g:i A'));
        }
        return $result;
    }

    /**
     * Show the sync started message right when they sync things.
     */
    private function showSyncStartedMessage() {
        $text = 'Starting the sync process…<br/>' .
                '<p id="sync-status-message">Please hang tight while we work our mojo. Sometimes the sync can take a while, ' .
                'especially on sites with lots of orders and/or products. You may refresh this page at ' .
                'anytime to check on the progress.</p>';

        add_settings_error('mail-in-one-woocommerce_notice', $this->plugin_name, __($text), 'updated');
    }

    private function campaign() {

        $campaign = new MailInOne_Woocommerce_MailInOneApi($this->getOption('mail_in_one_api_key'));
        return $campaign->getCampaign();
    }

    private function contactFilter() {
        $contactfilter = new MailInOne_Woocommerce_MailInOneApi($this->getOption('mail_in_one_api_key'));
        return $contactfilter->getContactFilter();
    }

    private function targetGroup() {
        $targetGroup = new MailInOne_Woocommerce_MailInOneApi($this->getOption('mail_in_one_api_key'));
        return $targetGroup->getTargetGroup();
    }

    private function DOIMailing() {
        $DOIMailing = new MailInOne_Woocommerce_MailInOneApi($this->getOption('mail_in_one_api_key'));
        return $DOIMailing->getDOIMailing();
    }

    private function contactEvents() {
        $contactEvents = new MailInOne_Woocommerce_MailInOneApi($this->getOption('mail_in_one_api_key'));
        return $contactEvents->getContactEvents();
    }

    private function customersFiled() {
        $customersFiled = new MailInOne_Woocommerce_MailInOneApi($this->getOption('mail_in_one_api_key'));
        return $customersFiled->getCustomersFiled();
    }

    /**
     * @param $input
     * @return array
     */
    protected function compileStoreInfoData($input) {
        return array(
            // store basics
            'store_name' => trim((isset($input['store_name']) ? $input['store_name'] : false)),
            'store_lastname' => trim((isset($input['store_lastname']) ? $input['store_lastname'] : false)),
            'store_street' => isset($input['store_street']) ? $input['store_street'] : false,
            'store_city' => isset($input['store_city']) ? $input['store_city'] : false,
            'store_state' => isset($input['store_state']) ? $input['store_state'] : false,
            'store_postal_code' => isset($input['store_postal_code']) ? $input['store_postal_code'] : false,
            'store_country' => isset($input['store_country']) ? $input['store_country'] : false,
            'store_phone' => isset($input['store_phone']) ? $input['store_phone'] : false,
            'admin_email' => isset($input['admin_email']) && is_email($input['admin_email']) ? $input['admin_email'] : $this->getOption('admin_email', false),
        );
    }

    /**
     * @param $input
     * @return array
     */
    protected function compileEmailNotificationData($input) {
        return array(
            // store basics
            'notification_email_notification' => isset($input['notification_email_notification']) ? $input['notification_email_notification'] : false,
            'notification_email_notification_list' => isset($input['notification_email_notification_list']) ? $input['notification_email_notification_list'] : false,
            'notification_email_subject' => isset($input['notification_email_subject']) ? $input['notification_email_subject'] : false,
            'notification_email_server' => isset($input['notification_email_server']) ? $input['notification_email_server'] : false,
            'notification_email_serverName' => isset($input['notification_email_serverName']) ? $input['notification_email_serverName'] : false,
            'notification_email_smtpHost' => isset($input['notification_email_smtpHost']) ? $input['notification_email_smtpHost'] : false,
            'notification_email_smtpUser' => isset($input['notification_email_smtpUser']) ? $input['notification_email_smtpUser'] : false,
            'notification_email_smtpPassword' => isset($input['notification_email_smtpPassword']) ? $input['notification_email_smtpPassword'] : false,
            'notification_email_smtpEncryption' => isset($input['notification_email_smtpEncryption']) ? $input['notification_email_smtpEncryption'] : false,
            'notification_email_smtpPort' => isset($input['notification_email_smtpPort']) ? $input['notification_email_smtpPort'] : false,
            'notification_email_fromEmail' => isset($input['notification_email_fromEmail']) ? $input['notification_email_fromEmail'] : false,
            'notification_email_fromName' => isset($input['notification_email_fromName']) ? $input['notification_email_fromName'] : false,
        );
    }

    /**
     * @param $input
     * @return array
     */
    protected function compileNewsletterData($input) {
        return array(
            'shortCode' => isset($input['shortCode']) ? $input['shortCode'] : false,
            'form_code-' . $input['shortCode'] => isset($input['form_code-' . $input['shortCode']]) ? $input['form_code-' . $input['shortCode']] : false,
        );
    }

    /**
     * @param $input
     * @return array
     */
    protected function compileStylingData($input) {

        $css = $this->getCSS($input['css_select-' . $input['shortCode']], $input['custom-' . $input['shortCode']], $input['button_background-' . $input['shortCode']], $input['button_font-' . $input['shortCode']]);
        return array(
            'shortCode' => isset($input['shortCode']) ? $input['shortCode'] : false,
            'css_select-' . $input['shortCode'] => isset($input['css_select-' . $input['shortCode']]) ? $input['css_select-' . $input['shortCode']] : false,
            'custom-' . $input['shortCode'] => isset($input['custom-' . $input['shortCode']]) ? $input['custom-' . $input['shortCode']] : false,
            'button_background-' . $input['shortCode'] => isset($input['button_background-' . $input['shortCode']]) ? $input['button_background-' . $input['shortCode']] : false,
            'button_font-' . $input['shortCode'] => isset($input['button_font-' . $input['shortCode']]) ? $input['button_font-' . $input['shortCode']] : false,
            'form_css-' . $input['shortCode'] => isset($input['css_select-' . $input['shortCode']]) ? strtolower($input['css_select-' . $input['shortCode']]) : false,
            'css-' . $input['shortCode'] => $css,
        );
    }

    /**
     * @param $input
     * @return array
     */
    protected function compileMessagesData($input) {
        return array(
            'shortCode' => isset($input['shortCode']) ? $input['shortCode'] : false,
            'success-' . $input['shortCode'] => isset($input['success-' . $input['shortCode']]) ? $input['success-' . $input['shortCode']] : false,
            'error-' . $input['shortCode'] => isset($input['error-' . $input['shortCode']]) ? $input['error-' . $input['shortCode']] : false,
            'success_url-' . $input['shortCode'] => isset($input['success_url-' . $input['shortCode']]) ? urlencode($input['success_url-' . $input['shortCode']]) : false,
            'agree_to_terms-' . $input['shortCode'] => isset($input['agree_to_terms-' . $input['shortCode']]) ? $input['agree_to_terms-' . $input['shortCode']] : false,
            'sbt_url-' . $input['shortCode'] => isset($input['sbt_url-' . $input['shortCode']]) ? urlencode($input['sbt_url-' . $input['shortCode']]) : false,
            'gdpr_url-' . $input['shortCode'] => isset($input['gdpr_url-' . $input['shortCode']]) ? urlencode($input['gdpr_url-' . $input['shortCode']]) : false,
            'company-' . $input['shortCode'] => isset($input['company-' . $input['shortCode']]) ? $input['company-' . $input['shortCode']] : false,
            'street-' . $input['shortCode'] => isset($input['street-' . $input['shortCode']]) ? $input['street-' . $input['shortCode']] : false,
            'postal_code-' . $input['shortCode'] => isset($input['postal_code-' . $input['shortCode']]) ? $input['postal_code-' . $input['shortCode']] : false,
            'city-' . $input['shortCode'] => isset($input['city-' . $input['shortCode']]) ? $input['city-' . $input['shortCode']] : false,
            'country-' . $input['shortCode'] => isset($input['country-' . $input['shortCode']]) ? $input['country-' . $input['shortCode']] : false,
            'email-' . $input['shortCode'] => isset($input['email-' . $input['shortCode']]) ? $input['email-' . $input['shortCode']] : false,
            'topics-' . $input['shortCode'] => isset($input['topics-' . $input['shortCode']]) ? $input['topics-' . $input['shortCode']] : false,
            'agree_to_terms-' . $input['shortCode'] => isset($input['agree_to_terms-' . $input['shortCode']]) ? $input['agree_to_terms-' . $input['shortCode']] : false,
            'standart_trems-' . $input['shortCode'] => isset($input['standart_trems-' . $input['shortCode']]) ? $input['standart_trems-' . $input['shortCode']] : false,
            'sbt_short-' . $input['shortCode'] => isset($input['sbt_short-' . $input['shortCode']]) ? urlencode($input['sbt_short-' . $input['shortCode']]) : false,
            'gdpr_short-' . $input['shortCode'] => isset($input['gdpr_short-' . $input['shortCode']]) ? urlencode($input['gdpr_short-' . $input['shortCode']]) : false,
        );
    }

    /**
     * @param null|array $data
     * @return bool
     */
    public function hasValidStoreInfo($data = null) {
        return $this->validateOptions(array(
                    'store_name', 'store_lastname', 'store_street', 'store_city', 'store_state',
                    'store_postal_code', 'store_country', 'store_phone',
                    'admin_email',
                        ), $data);
    }

    /**
     * @param null|array $data
     * @return bool
     */
    public function hasValidEmailNotification($data = null) {
        return $this->validateOptions(array(
                    'notification_email_notification', 'notification_email_notification_list', 'notification_email_subject', 'notification_email_server',
                        ), $data);
    }

    /**
     * @param $data
     * @return bool
     */
    protected function hasInvalidStorePhone($data) {
        if (empty($data['store_phone']) || strlen($data['store_phone']) <= 6) {
            return true;
        }

        return false;
    }

    /**
     *
     */
    protected function addInvalidPhoneAlert() {
        add_settings_error('mailInOne_store_settings', '', 'Email is out and you will be contacted with confirmation of your user login, password and API Key');
    }

    protected function getCSS($css_select, $custom, $button_background, $button_font) {

        switch ($css_select) {
            case 'CSS1':
                $css = $this->css1($button_background, $button_font);
                break;

            case 'CSS2':
                $css = $this->css2($button_background, $button_font);
                break;

            case 'CSS3':
                $css = $this->css3($button_background, $button_font);
                break;

            case 'Custom':
                $css = '<style>' . $custom . '</style>';
                break;

            default :
                $css = '';
                break;
        }
        return $css;
    }

    protected function css1($button_background, $button_font) {

        $css1 = '<style>' .
                '.css1 label {' .
                'display: inline-block;' .
                'text-align: right;' .
                '}' .
                '.css1 input[type=text], .css1 input[type=email] {' .
                'padding: 10px 10px;' .
                'margin: 8px 0;' .
                'box-sizing: border-box;' .
                '}' .
                '.css1 input[type=text]:focus {' .
                'border: 3px solid #21a12d;' .
                '}' .
                '.css1 select {' .
                'border: none;' .
                'border-radius: 4px;' .
                'background-color: #f1f1f1;' .
                '}' .
                '.css1 input[type=button], .css1 input[type=submit], .css1 input[type=reset] {' .
                'background-color: ' . $button_background . '; ' .
                'border: none;' .
                'color: white;' .
                'padding: 16px 32px;' .
                'text-decoration: none;' .
                'margin: 4px 2px;' .
                'cursor: pointer;' .
                'font-size: ' . $button_font . '; ' .
                '}' .
                '::-webkit-input-placeholder {' .
                'color:transparent;' .
                '}' .
                ':-moz-placeholder {' .
                'color:transparent;' .
                '}' .
                '::-moz-placeholder {' .
                'color:transparent; ' .
                '}' .
                ':-ms-input-placeholder { ' .
                'color:transparent;' .
                '}' .
                '</style>';

        return $css1;
    }

    protected function css2($button_background, $button_font) {

        $css2 = '<style>' .
                '.css2 label, .css2 input {' .
                'display:block;' .
                '}' .
                '.css2 input[type=text], .css2 input[type=email] {' .
                'padding: 10px 10px;' .
                'margin: 8px 0;' .
                'box-sizing: border-box;' .
                '}' .
                '.css2 input[type=text]:focus {' .
                'border: 3px solid #21a12d;' .
                '}' .
                '.css2 select {' .
                'border: none;' .
                'border-radius: 4px;' .
                'background-color: #f1f1f1;' .
                '}' .
                '.css2 input[type=button], .css2 input[type=submit], .css2 input[type=reset] {' .
                'background-color:' . $button_background . '; ' .
                'border: none;' .
                'color: white;' .
                ' padding: 16px 32px;' .
                ' text-decoration: none;' .
                ' margin: 4px 2px;' .
                ' cursor: pointer;' .
                'font-size: ' . $button_font . '; ' .
                '}' .
                '::-webkit-input-placeholder {' .
                'color:transparent;' .
                '}' .
                ':-moz-placeholder {' .
                'color:transparent;' .
                '}' .
                '::-moz-placeholder {' .
                'color:transparent; ' .
                '}' .
                ':-ms-input-placeholder { ' .
                'color:transparent;' .
                '}' .
                '</style>';
        return $css2;
    }

    protected function css3($button_background, $button_font) {
        $css3 = '<style>' .
                '.css3 label {' .
                'display:none;' .
                '}' .
                ' .css3 input[type=text], .css3 input[type=email] {' .
                ' padding: 10px 10px;' .
                'margin: 8px 0;' .
                'box-sizing: border-box;' .
                '}' .
                '.css3 input[type=text]:focus {' .
                'border: 3px solid #21a12d;' .
                '}' .
                '.css3 select {' .
                'border: none;' .
                'border-radius: 4px;' .
                ' background-color: #f1f1f1;' .
                '}' .
                '.css3 input[type=button], .css3 input[type=submit], .css3 input[type=reset] {' .
                'background-color: #21a12d;' .
                'border: none;' .
                'color: white;' .
                'padding: 16px 32px;' .
                'text-decoration: none;' .
                'margin: 4px 2px;' .
                'cursor: pointer;' .
                '}' .
                '</style>';


        return $css3;
    }

}
