
<h2 style="padding-top: 1em;">E-Mail Notifikation</h2>
<p>Comma-separated list of email addresses to send message</p>
<fieldset>
    <legend class="screen-reader-text">
        <span>Send Email Notation</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-notification">
     <select id="<?php echo $this->plugin_name; ?>-showCode" name="<?php echo $this->plugin_name; ?>[notification_email_notification]" >
     <option value="" >--Please choose--</option> 
     
       <?php
            if (!empty($options['notification_email_notification']) ) {
               ?>
            <option value="<?php echo $options['notification_email_notification'] ;?>"  <?php echo 'selected'; ?>><?php echo $options['notification_email_notification'];?></option>
            <?php
            }
            ?>
             <option value="True" >True</option>
              <option value="False" >False</option>
    </select>
    <?php
     esc_attr_e('Send Email Notation', $this->plugin_name);
    ?>
    </label>
</fieldset>


<fieldset>
    <legend class="screen-reader-text">
        <span>Email Notification List</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-notification-list">
        <input required style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-notification-email-notification-list" name="<?php echo $this->plugin_name; ?>[notification_email_notification_list]" value="<?php echo isset($options['notification_email_notification_list']) ? $options['notification_email_notification_list'] : '' ?>" />
        <span>
            <?php
            if (!empty($options['notification_email_notification_list']) ) {
                esc_attr_e('Email Notification List', $this->plugin_name);   
            } else {
                esc_attr_e('Email Notification List', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>

<fieldset>
    <legend class="screen-reader-text">
        <span>Email Subject</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-subject">
        <input required style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-notification-email-subject" name="<?php echo $this->plugin_name; ?>[notification_email_subject]" value="<?php echo isset($options['notification_email_subject']) ? $options['notification_email_subject'] : '' ?>" />
        <span>
            <?php
            if (!empty($options['notification_email_subject']) ) {
                esc_attr_e('Email Subject', $this->plugin_name);
            } else {
                esc_attr_e('Email Subject', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>

<fieldset>
    <legend class="screen-reader-text">
        <span>WP Or SMTP-Server</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-server">
     <select id="<?php echo $this->plugin_name; ?>-notification-email-server" name="<?php echo $this->plugin_name; ?>[notification_email_server]" >
     <option value="" >--Please choose--</option> 
     
       <?php
            if (!empty($options['notification_email_server']) ) {
               ?>
            <option value="<?php echo $options['notification_email_server'] ;?>"  <?php echo 'selected'; ?>><?php echo $options['notification_email_server'];?></option>
            <?php
            }
            ?>
             <option value="WP-Server" >WP-Server</option>
              <option value="SMTP-Server" >SMTP-Server</option>
    </select>
    <?php
     esc_attr_e('WP Or SMTP-Server', $this->plugin_name);
    ?>
    </label>
</fieldset>

<p>SMTP-Server</p>
<fieldset>
    <legend class="screen-reader-text">
        <span>server Name</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-serverName">
        <input style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-notification-email-serverName" name="<?php echo $this->plugin_name; ?>[notification_email_serverName]" value="<?php echo isset($options['notification_email_serverName']) ? $options['notification_email_serverName'] : '' ?>" />
        <span>
            <?php
                esc_attr_e('server Name', $this->plugin_name);         
            ?>
        </span>
    </label>
</fieldset>
<fieldset>
    <legend class="screen-reader-text">
        <span>SMTP Host</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-smtpHost">
        <input style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-notification-email-smtpHost" name="<?php echo $this->plugin_name; ?>[notification_email_smtpHost]" value="<?php echo isset($options['notification_email_smtpHost']) ? $options['notification_email_smtpHost'] : '' ?>" />
        <span>
            <?php
                esc_attr_e('SMTP Host', $this->plugin_name);         
            ?>
        </span>
    </label>
</fieldset>
<fieldset>
    <legend class="screen-reader-text">
        <span>SMTP User</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-smtpUser">
        <input style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-notification-email-smtpUser" name="<?php echo $this->plugin_name; ?>[notification_email_smtpUser]" value="<?php echo isset($options['notification_email_smtpUser']) ? $options['notification_email_smtpUser'] : '' ?>" />
        <span>
            <?php
                esc_attr_e('SMTP User', $this->plugin_name);        
            ?>
        </span>
    </label>
</fieldset>
<fieldset>
    <legend class="screen-reader-text">
        <span>SMTP Password</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-smtpPassword">
        <input style="width: 30%;" type="password" id="<?php echo $this->plugin_name; ?>-notification-email-smtpPassword" name="<?php echo $this->plugin_name; ?>[notification_email_smtpPassword]" value="<?php echo isset($options['notification_email_smtpPassword']) ? $options['notification_email_smtpPassword'] : '' ?>" />
        <span>
            <?php
                esc_attr_e('SMTP Password', $this->plugin_name);        
            ?>
        </span>
    </label>
</fieldset>
<fieldset>
    <legend class="screen-reader-text">
        <span>SMTP Encryption</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-smtpEncryption">
  <select style="width: 30%;" id="<?php echo $this->plugin_name; ?>-notification_email_smtpEncryption" name="<?php echo $this->plugin_name; ?>[notification_email_smtpEncryption]" >                 
                <option value="1" <?php echo $options['notification_email_smtpEncryption'] == "1" ? 'selected' : '' ?>>YES</option>
                <option value="0" <?php echo $options['notification_email_smtpEncryption'] == "0" ? 'selected' : '' ?>>NO</option>  
                </select>
        <span>
            <?php
                esc_attr_e('SMTP TLS', $this->plugin_name);
            ?>
        </span>
    </label>
</fieldset>
<fieldset>
    <legend class="screen-reader-text">
        <span>SMTP Port</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-smtpPort">
        <input style="width: 30%;" type="number" id="<?php echo $this->plugin_name; ?>-notification-email-smtpPort" name="<?php echo $this->plugin_name; ?>[notification_email_smtpPort]" value="<?php echo isset($options['notification_email_smtpPort']) ? $options['notification_email_smtpPort'] : '' ?>" />
        <span>
            <?php
                esc_attr_e('SMTP Port', $this->plugin_name);        
            ?>
        </span>
    </label>
</fieldset>
<fieldset>
    <legend class="screen-reader-text">
        <span>From Email</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-fromEmail">
        <input style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-notification-email-fromEmail" name="<?php echo $this->plugin_name; ?>[notification_email_fromEmail]" value="<?php echo isset($options['notification_email_fromEmail']) ? $options['notification_email_fromEmail'] : '' ?>" />
        <span>
            <?php 
                esc_attr_e('From Email', $this->plugin_name);
            ?>
        </span>
    </label>
</fieldset>
<fieldset>
    <legend class="screen-reader-text">
        <span>From Name</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-notification-email-fromName">
        <input style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-notification-email-fromName" name="<?php echo $this->plugin_name; ?>[notification_email_fromName]" value="<?php echo isset($options['notification_email_fromName']) ? $options['notification_email_fromName'] : '' ?>" />
        <span>
            <?php
                esc_attr_e('From Name', $this->plugin_name);
            ?>
        </span>
    </label>
</fieldset>
<?php submit_button('Save', 'primary','submit', TRUE); ?>

</form>

