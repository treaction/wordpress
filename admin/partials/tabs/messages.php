
<?php
    if(isset($_GET['id']) && !empty($_GET['id'])){        
        $shortCode = $_GET['id'];
    }else{
        $shortCode = 0;
    }
    $sowTable = $handler->showTable();
?>
<script language="javascript" type="text/javascript">
     function getShortCode(){
      var shortCode = document.getElementById("<?php echo $this->plugin_name; ?>-showCode").value;  
      window.location = "?page=mail-in-one-woocommerce&tab=messages&id="+shortCode;
    }
 </script>  
 
<style>
  .form-table th {

    vertical-align: top;
    text-align: left;
    padding: 20px 10px 20px 0;
    width: 200px;
    line-height: 1.3;
    font-weight: 600;
    font-size: 14px;

}
label {

    color: #23282d;
    font-weight: 400;
    text-shadow: none;
    vertical-align: baseline;

}
  
input[type="text"], input[type="search"], input[type="radio"], input[type="tel"], input[type="time"], input[type="url"], input[type="week"], input[type="password"], input[type="checkbox"], input[type="color"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="email"], input[type="month"], input[type="number"], select, textarea {

    border: 1px solid #ddd;
    box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
    background-color: #fff;
    color: #32373c;
    outline: 0;
    transition: 50ms border-color ease-in-out;
    padding: 3px 5px;

}
.widefat {

    border-spacing: 0;
    width: 100%;
    clear: both;
    margin: 0;

}
    
</style>
<?php

$Company = isset($options['company-'.$shortCode]) && !empty($options['company-'.$shortCode]) ? $options['company-'.$shortCode] : '$Company';
$Street = isset($options['street-'.$shortCode]) && !empty($options['street-'.$shortCode]) ? $options['street-'.$shortCode] : '$Street';
$Postal_Code = isset($options['postal_code-'.$shortCode]) && !empty($options['postal_code-'.$shortCode]) ? $options['postal_code-'.$shortCode] : '$Postal_Code';
$City = isset($options['city-'.$shortCode]) && !empty($options['city-'.$shortCode]) ? $options['city-'.$shortCode] : '$City';
$Country = isset($options['country-'.$shortCode]) && !empty($options['country-'.$shortCode]) ? $options['country-'.$shortCode] : '$Country';
$Topics = isset($options['topics-'.$shortCode]) && !empty($options['topics-'.$shortCode]) ? $options['topics-'.$shortCode] : '$Topics';
$Email = isset($options['email-'.$shortCode]) && !empty($options['email-'.$shortCode]) ? $options['email-'.$shortCode] : '$Email';
if(isset($options['sbt_url-'.$shortCode]) && !empty($options['sbt_url-'.$shortCode])){
    $sbt_url = '<a target=_blank href='.urldecode($options['sbt_url-'.$shortCode]).'>'.  $options['sbt_short-'.$shortCode] .'</a>';
}else{
    $sbt_url = $options['sbt_short-'.$shortCode];
}
if(isset($options['gdpr_url-'.$shortCode]) && !empty($options['gdpr_url-'.$shortCode])){
    $gdpr_url = '<a target=_blank href='.urldecode($options['gdpr_url-'.$shortCode]).'>'.  $options['gdpr_short-'.$shortCode] .'</a>';
}else{
    $gdpr_url = $options['gdpr_short-'.$shortCode];
}

$standart_trems = "Ich, erkläre mich mit den ".$sbt_url." und ".$gdpr_url." der ".$Company.", " . $Street." ".$Postal_Code ." ".$City ." ".$Country ." einverstanden.\n"
                . "Ich bin einverstanden, dass die ".$Company.", meine eingetragenen Daten nutzt und mir E-Mails zuschickt bzw.\n"
                . "mich anruft, um mir Informationen zu den Bereichen ". $Topics ." zukommen zu lassen.\n"
                . "Diese Einwilligung kann ich jederzeit widerrufen,\n"
                . "etwa durch einen Brief an die oben genannte Adresse oder durch\n"
                . "eine E-Mail an ".$Email." Anschließend wird jede werbliche Nutzung unterbleiben.\n"; 
$options['standart_trems-'.$shortCode] = $standart_trems;
?>

<input type="hidden" name="mail_in_one_active_settings_tab"  value="messages"/>
<input type="hidden" name="<?php echo $this->plugin_name; ?>[shortCode]" id="<?php echo $this->plugin_name; ?>-shortCode" value="<?php echo $shortCode;?>" />
<input type="hidden" id="<?php echo $this->plugin_name; ?>-standart_trems" name="<?php echo $this->plugin_name; ?>[standart_trems]" value="<?php #echo $standart_trems;?>"  />
<table class="form-table">
     <tr valign="top">
        <th scope="row"><label>Short Code</label></th>
        <td>
            <select id="<?php echo $this->plugin_name; ?>-showCode" name="<?php echo $this->plugin_name; ?>[showCode]" onchange="getShortCode()">
     <option value="" >--Please choose an Short Code--</option> 
      <?php foreach ($sowTable as $showRow) {
          if($showRow->status == 'Activ'){
          ?>    
     <option value="<?php echo $showRow->id ;?>"  <?php echo $showRow->id == $shortCode?  'selected'  : ''; ?>><?php echo $showRow->name;?></option> 
          <?php  }}?>
 </select>
        </td>
    </tr>
</table>
<h2>Text</h2>

<table class="form-table">
	
	<tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-success-<?php echo $shortCode;?>">Success</label></th>
		<td>
			<input type="text" class="widefat" id="<?php echo $this->plugin_name; ?>-success-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[success-<?php echo $shortCode;?>]" value="<?php echo isset($options['success-'.$shortCode]) ? $options['success-'.$shortCode] : ''; ?>"  />
			<p class="help">The text that shows when an email address is successfully subscribed</p>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-error-<?php echo $shortCode;?>">Error</label></th>
		<td>
			<input type="text" class="widefat" id="<?php echo $this->plugin_name; ?>-error-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[error-<?php echo $shortCode;?>]" value="<?php echo isset($options['error-'.$shortCode]) ? $options['error-'.$shortCode] : ''; ?>" />
			<p class="help">The text that shows when a error occured.</p>
		</td>
	</tr>
         <tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-agree_to_terms-<?php echo $shortCode;?>">Agree to Terms</label></th>
		<td>
                    <textarea style="position: relative; overflow: hidden; background: white;"  cols="70" rows="15" id="<?php echo $this->plugin_name; ?>-agree_to_terms-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[agree_to_terms-<?php echo $shortCode;?>]"><?php echo isset($options['agree_to_terms-'.$shortCode]) && !empty($options['agree_to_terms-'.$shortCode]) ? htmlspecialchars($options['agree_to_terms-'.$shortCode], ENT_QUOTES, get_option('blog_charset')) : $standart_trems; ?></textarea>
		<p class="help">After Updating parameters like company, street etc., please delete the input box and “save all changes”. A new “agree to terms” statement will be generated automatically</p>
                </td>
	</tr>
		
</table>

<h1>URL</h1>

<table class="form-table">
	
	<tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-success_url-<?php echo $shortCode;?>">Success URL</label></th>
		<td>
                    <input type="text"  class="widefat" id="<?php echo $this->plugin_name; ?>-success_url-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[success_url-<?php echo $shortCode;?>]" value="<?php echo isset($options['success_url-'.$shortCode]) ? urldecode($options['success_url-'.$shortCode])  : ''; ?>" />
			<p class="help">The page that shows when an email address is successfully subscribed</p>
		</td>
	</tr>
       <tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-sbt_url-<?php echo $shortCode;?>">Standard Business Terms</label></th>
		<td>
                    <input type="text"  class="widefat" id="<?php echo $this->plugin_name; ?>-sbt_url-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[sbt_url-<?php echo $shortCode;?>]" value="<?php echo isset($options['sbt_url-'.$shortCode]) ? urldecode($options['sbt_url-'.$shortCode])  : ''; ?>" />
			
		</td>
	</tr>
           <tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-sbt_short-<?php echo $shortCode;?>">Short Name(SBT)</label></th>
		<td>
                    <input type="text"  class="widefat" id="<?php echo $this->plugin_name; ?>-sbt_short-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[sbt_short-<?php echo $shortCode;?>]" value="<?php echo isset($options['sbt_short-'.$shortCode]) ? urldecode($options['sbt_short-'.$shortCode])  : 'AGB'; ?>" />
			
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-gdpr_url-<?php echo $shortCode;?>">GDPR</label></th>
		<td>
                    <input type="text"  class="widefat" id="<?php echo $this->plugin_name; ?>-gdpr_url-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[gdpr_url-<?php echo $shortCode;?>]" value="<?php echo isset($options['gdpr_url-'.$shortCode]) ? urldecode($options['gdpr_url-'.$shortCode])  : ''; ?>" />
			
		</td>
	</tr>
         <tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-gdpr_short-<?php echo $shortCode;?>">Short Name(GDPR)</label></th>
		<td>
                    <input type="text"  class="widefat" id="<?php echo $this->plugin_name; ?>-gdpr_short-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[gdpr_short-<?php echo $shortCode;?>]" value="<?php echo isset($options['gdpr_short-'.$shortCode]) ? urldecode($options['gdpr_short-'.$shortCode])  : 'Datenschutz'; ?>" />
			
		</td>
	</tr>
		
</table>

<h2>Data</h2>

<table class="form-table">
	
	<tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-company-<?php echo $shortCode;?>">Company</label></th>
		<td>
			<input type="text" class="widefat" id="<?php echo $this->plugin_name; ?>-company-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[company-<?php echo $shortCode;?>]"    value="<?php echo isset($options['company-'.$shortCode]) ? $options['company-'.$shortCode] : ''; ?>" />			
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-street-<?php echo $shortCode;?>">Street</label></th>
		<td>
			<input type="text" class="widefat" id="<?php echo $this->plugin_name; ?>-street-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[street-<?php echo $shortCode;?>]"    value="<?php echo isset($options['street-'.$shortCode]) ? $options['street-'.$shortCode] : '' ?>" />
			
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-postal_code-<?php echo $shortCode;?>">Postal-Code</label></th>
		<td>
                    <input type="text" class="widefat" id="<?php echo $this->plugin_name; ?>-postal_code-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[postal_code-<?php echo $shortCode;?>]"    value="<?php echo isset($options['postal_code-'.$shortCode]) ? urldecode($options['postal_code-'.$shortCode])  : ''; ?>" />
			
		</td>
	</tr>
        <tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-city-<?php echo $shortCode;?>">City</label></th>
		<td>
			<input type="text" class="widefat" id="<?php echo $this->plugin_name; ?>-city-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[city-<?php echo $shortCode;?>]"    value="<?php echo isset($options['city-'.$shortCode]) ? $options['city-'.$shortCode] : ''; ?>" />
			
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-country-<?php echo $shortCode;?>">Country</label></th>
		<td>
			<input type="text" class="widefat" id="<?php echo $this->plugin_name; ?>-country-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[country-<?php echo $shortCode;?>]"    value="<?php echo isset($options['country-'.$shortCode]) ? $options['country-'.$shortCode] : ''; ?>"  />
			
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-email-<?php echo $shortCode;?>">Email</label></th>
		<td>
                    <input type="email" class="widefat" id="<?php echo $this->plugin_name; ?>-email-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[email-<?php echo $shortCode;?>]"    value="<?php echo isset($options['email-'.$shortCode]) ? $options['email-'.$shortCode]  : ''; ?>" />
			
		</td>
	</tr>
        <tr valign="top">
		<th scope="row"><label for="<?php echo $this->plugin_name; ?>-topics-<?php echo $shortCode;?>">Topics</label></th>
		<td>
                     <textarea style="position: relative; overflow: hidden; background: white;"  cols="70" rows="15" id="<?php echo $this->plugin_name; ?>-topics-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[topics-<?php echo $shortCode;?>]"><?php echo htmlspecialchars($options['topics-'.$shortCode], ENT_QUOTES, get_option('blog_charset')); ?></textarea>
		</td>
	</tr>
		
		
</table>
