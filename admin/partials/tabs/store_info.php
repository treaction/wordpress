
<input type="hidden" name="mail_in_one_active_settings_tab"  value="store_info"/>

<h2 style="padding-top: 1em;">create a new account</h2>
<p>Apply for an API Key by filling out this form. We will send you an API Key by email</p>
<fieldset>
    <legend class="screen-reader-text">
        <span>First name</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-store-name-label">
        <input required style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-store-name-label" name="<?php echo $this->plugin_name; ?>[store_name]" value="<?php echo isset($options['store_name']) ? $options['store_name'] : '' ?>" />
        <span>
            <?php
            if (!empty($options['store_name']) ) {
                esc_attr_e('First name', $this->plugin_name);
            } else {
                esc_attr_e('First name', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>


<fieldset>
    <legend class="screen-reader-text">
        <span>Last name</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-store-lastname-label">
        <input required style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-store-nachname-label" name="<?php echo $this->plugin_name; ?>[store_lastname]" value="<?php echo isset($options['store_lastname']) ? $options['store_lastname'] : '' ?>" />
        <span>
            <?php
            if (!empty($options['store_lastname']) ) {
                esc_attr_e('Last name', $this->plugin_name);
            } else {
                esc_attr_e('Last name', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>

<fieldset>
    <legend class="screen-reader-text">
        <span>Email</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-admin-email-label">
        <input required style="width: 30%;" type="email" id="<?php echo $this->plugin_name; ?>-admin-email-label" name="<?php echo $this->plugin_name; ?>[admin_email]" value="<?php echo isset($options['admin_email']) ? $options['admin_email'] : get_option('admin_email') ?>" />
        <span>
            <?php
            if (!empty($options['admin_email']) ) {
                esc_attr_e('Email', $this->plugin_name);
            } else {
                esc_attr_e('Email', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>

<fieldset>
    <legend class="screen-reader-text">
        <span>Street Address</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-store-address-label">
        <input required style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-store-address-label" name="<?php echo $this->plugin_name; ?>[store_street]" value="<?php echo isset($options['store_street']) ? $options['store_street'] : '' ?>" />
        <span>
            <?php
            if (!empty($options['store_street']) ) {
                esc_attr_e('Street address', $this->plugin_name);
            } else {
                esc_attr_e('Street address', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>

<fieldset>
    <legend class="screen-reader-text">
        <span>City</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-store-city-label">
        <input required style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-store-city-label" name="<?php echo $this->plugin_name; ?>[store_city]" value="<?php echo isset($options['store_city']) ? $options['store_city'] : '' ?>" />
        <span>
            <?php
            if (!empty($options['store_city']) ) {
                esc_attr_e('City', $this->plugin_name);
            } else {
                esc_attr_e('City', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>

<fieldset>
    <legend class="screen-reader-text">
        <span>State</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-store-state-label">
        <input required style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-store-state-label" name="<?php echo $this->plugin_name; ?>[store_state]" value="<?php echo isset($options['store_state']) ? $options['store_state'] : '' ?>" />
        <span>
            <?php
            if (!empty($options['store_state']) ) {
                esc_attr_e('State', $this->plugin_name);
            } else {
                esc_attr_e('State', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>

<fieldset>
    <legend class="screen-reader-text">
        <span>Postal Code</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-store-state-label">
        <input required style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-store-postal-code-label" name="<?php echo $this->plugin_name; ?>[store_postal_code]" value="<?php echo isset($options['store_postal_code']) ? $options['store_postal_code'] : '' ?>" />
        <span>
            <?php
            if (!empty($options['store_postal_code']) ) {
                esc_attr_e('Postal Code', $this->plugin_name);
            } else {
                esc_attr_e('Postal Code', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>

<fieldset>
    <legend class="screen-reader-text">
        <span>Country</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-store-country-label">
        <input required style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-store-country-label" name="<?php echo $this->plugin_name; ?>[store_country]" value="<?php echo isset($options['store_country']) ? $options['store_country'] : '' ?>" />
        <span>
            <?php
            if (!empty($options['store_country'])) {
                esc_attr_e('Country', $this->plugin_name);
            } else {
                esc_attr_e('Country', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>

<fieldset>
    <legend class="screen-reader-text">
        <span>Phone</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-store-phone-label">
        <input required style="width: 30%;" type="text" id="<?php echo $this->plugin_name; ?>-store-phone-label" name="<?php echo $this->plugin_name; ?>[store_phone]" value="<?php echo isset($options['store_phone']) ? $options['store_phone'] : '' ?>" />
        <span>
            <?php
            if (!empty($options['store_phone']) ) {
                esc_attr_e('Phone Number', $this->plugin_name);
            } else {
                esc_attr_e('Phone Number', $this->plugin_name); echo '<span style="color:red;">*</span>';
            }
            ?>
        </span>
    </label>
</fieldset>

<?php submit_button('Send Request', 'primary','submit', TRUE); ?>

</form>

