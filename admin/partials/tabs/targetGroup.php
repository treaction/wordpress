<?php

if (isset($options['mail_in_one_api_key']) && !$handler->hasValidApiKey()) {
    include_once __DIR__.'/errors/missing_api_key.php';
}

$targetGroups = $handler->getTargetGroup();

?>
<table class="wp-list-table widefat fixed striped pages">
<thead>
<tr>
<th>ID</th>
<th>Name</th>
<th>Aktive Kontakte</th>
<th>Kontakte</th>
<th>Erstellung</th>
<th>Aktualisierung</th>
</tr>
</thead>
<tbody>
<?php
foreach ($targetGroups->getResult() as $targetGroup){
    ?>
  <tr>
    <td><?php echo $targetGroup->id; ?></td>
    <td><?php echo $targetGroup->name;?></td>
    <td><?php echo $targetGroup->countActiveContacts; ?></td>
    <td><?php echo $targetGroup->countContacts;?></td>
    <td><?php echo $targetGroup->created; ?></td>
    <td><?php echo $targetGroup->updated;?></td>
  </tr>
    
<?php 
}
?> 
</tbody>
</table>
