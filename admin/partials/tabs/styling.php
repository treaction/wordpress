<?php
    if(isset($_GET['id']) && !empty($_GET['id'])){        
        $shortCode = $_GET['id'];
    }else{
        $shortCode = 0;
    }
    $sowTable = $handler->showTable();
?>
<script language="javascript" type="text/javascript">
     function getShortCode(){
      var shortCode = document.getElementById("<?php echo $this->plugin_name; ?>-showCode").value;  
      window.location = "?page=mail-in-one-woocommerce&tab=styling&id="+shortCode;
    }
 </script>   

 
<input type="hidden" name="<?php echo $this->plugin_name; ?>[shortCode]" id="<?php echo $this->plugin_name; ?>-shortCode" value="<?php echo $shortCode;?>" />
<h1>Form Styling</h1>

<table class="form-table">
     <tr valign="top">
        <th scope="row"><label>Short Code</label></th>
        <td>
            <select id="<?php echo $this->plugin_name; ?>-showCode" name="<?php echo $this->plugin_name; ?>[showCode]" onchange="getShortCode()">
     <option value="" >--Please choose an Short Code--</option> 
      <?php foreach ($sowTable as $showRow) {
          if($showRow->status == 'Activ'){
          ?>    
     <option value="<?php echo $showRow->id ;?>"  <?php echo $showRow->id == $shortCode?  'selected'  : ''; ?>><?php echo $showRow->name;?></option> 
          <?php  }}?>
 </select>
        </td>
    </tr>

    <tr valign="top">
        <th scope="row"><label for="<?php echo $this->plugin_name; ?>-styling">Styling</label></th>
        <td>
            <select name="<?php echo $this->plugin_name; ?>[css_select-<?php echo $shortCode;?>]">
                <option value="">--Please choose an option--</option>
                <option value="Standard" <?php echo $options['css_select-'.$shortCode] == 'Standard'?  'selected'  : ''; ?>>Standard</option>
                <option value="CSS1" <?php echo $options['css_select-'.$shortCode] == 'CSS1'?  'selected'  : ''; ?>>Label left and Input field right</option>
                <option value="CSS2" <?php echo $options['css_select-'.$shortCode] == 'CSS2'?  'selected'  : ''; ?>>Label above and Input field below</option>
                <option value="CSS3" <?php echo $options['css_select-'.$shortCode] == 'CSS3'?  'selected'  : ''; ?>>Label hidden as Text in Input field</option>
                <option value="Custom" <?php echo $options['css_select-'.$shortCode] == 'Custom'?  'selected'  : ''; ?>>Custom Form</option>
            </select>
        </td>
    </tr>
     <tr valign="top">
        <th scope="row"><label for="<?php echo $this->plugin_name; ?>-custom-<?php echo $shortCode;?>">Custom CSS</label></th>
        <td>
           <textarea style="position: relative; background: white;"  cols="100" rows="10" id="<?php echo $this->plugin_name; ?>-custom-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[custom-<?php echo $shortCode;?>]"><?php echo $options['custom-'.$shortCode]; ?></textarea>    
        </td>
    </tr>
     <tr valign="top">
        <th scope="row"><label for="<?php echo $this->plugin_name; ?>-button_background-<?php echo $shortCode;?>">HexCode Button Background</label></th>
        <td>
          <input type="text" class="widefat" id="<?php echo $this->plugin_name; ?>-button_background-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[button_background-<?php echo $shortCode;?>]" value="<?php echo isset($options['button_background-'.$shortCode]) ? $options['button_background-'.$shortCode] : ''; ?>"  />
        </td>
    </tr>
     <tr valign="top">
        <th scope="row"><label for="<?php echo $this->plugin_name; ?>-button_font-<?php echo $shortCode;?>">Button Font Size</label></th>
        <td>           
           <input type="text" class="widefat" id="<?php echo $this->plugin_name; ?>-button_font-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[button_font-<?php echo $shortCode;?>]" value="<?php echo isset($options['button_font-'.$shortCode]) ? $options['button_font-'.$shortCode] : ''; ?>"  />
        </td>
    </tr>
</table>
