<?php

if (isset($options['mail_in_one_api_key']) && !$handler->hasValidApiKey()) {
    include_once __DIR__.'/errors/missing_api_key.php';
}
$conatctFilters = $handler->getContactFilter();

?>
<table class="wp-list-table widefat fixed striped pages">
<thead>
<tr>
<th>ID</th>
<th>Name</th>
<th>Kontakte</th>
<th>Erstellung</th>
</tr>
</thead>
<tbody>
<?php
foreach ($conatctFilters->getResult() as $conatctFilter){
    ?>
  <tr>
    <td><?php echo $conatctFilter->id; ?></td>
    <td><?php echo $conatctFilter->name;?></td>
    <td><?php echo $conatctFilter->countContacts;?></td>
    <td><?php echo $conatctFilter->created; ?></td>
  </tr>
    
<?php 
}
?> 
</tbody>
</table>