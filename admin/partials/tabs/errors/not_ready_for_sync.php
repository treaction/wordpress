<div class="error notice is-dismissable">
    <p><?php _e('Mail In One says: You are not fully ready to run the Store Sync, please verify your settings before proceeding.', 'mail-in-one-woocommerce'); ?></p>
</div>
