<?php



$store_syncing = false;
$last_updated_time = get_option('mail-in-one-woocommerce-resource-last-updated');
$last_updated_contacts = get_option('mail-in-one-woocommerce-resource-last-updated-contacts');


if (!empty($last_updated_time)) {
   $last_updated_time = mail_in_one_date_local($last_updated_time);
}

if(mail_in_one_get_api()) {
   #$store_syncing = true;
}

$handler =  MailInOne_Woocommerce_Admin::connect();
$res = $handler->startSync();
#var_dump($res);

?>

<input type="hidden" name="mail_in_one_active_settings_tab" value="store_sync"/>

<?php if($store_syncing): ?>
    <h2 style="padding-top: 1em;">Sync Progress</h2>
<?php endif; ?>

<?php if(!$store_syncing): ?>
    <h2 style="padding-top: 1em;">Sync Status</h2>
<?php endif; ?>

<p>
    <strong>synchronized contacts:</strong> <?php echo $last_updated_contacts; ?>
</p>


<?php if ($last_updated_time): ?>
    <p><strong>Last Updated: OK </strong> <i><?php echo $last_updated_time->format('D, M j, Y g:i A'); ?></i></p>
<?php endif; ?>

<?php if(mail_in_one_get_api() && (!$store_syncing || isset($_GET['resync']) && $_GET['resync'] === '1')): ?>

    <?php submit_button('Resync', 'primary','submit', TRUE); ?>
<?php endif; ?>
</form>