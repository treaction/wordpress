 <?php

    if(isset($_GET['id']) && !empty($_GET['id'])){        
        $shortCode = $_GET['id'];
    }else{
        $shortCode = 0;
    }
    $terms = '';
    if(!empty($options['agree_to_terms-'.$shortCode]) && isset($options['agree_to_terms-'.$shortCode])){
        $terms = nl2br($options['agree_to_terms-'.$shortCode]);
    }else{
         $terms = nl2br($options['standart_trems-'.$shortCode]);
    }
    
      $termsJS = str_replace('<br />', '\\', $terms);
    
   $sowTable = $handler->showTable();

function addtext2($input,$termsJS){
   $text = '';
   $label = '';
   $inputFiled = '';
   $submit = '';
     switch ($input) {    
    case "email":
        $label = "Email&nbsp;Addess *";
        $inputFiled = '<input name=email placeholder='.$label.' type=email required >\r</p>';
        break;
    case "firstName":
        $label = "First&nbsp;Name";
         $inputFiled = '<input name=FIRSTNAME placeholder='.$label.' type=text>\r</p>';
        break;
    case "lastName":
        $label = "Last&nbsp;Name";
         $inputFiled = '<input name=LASTNAME placeholder='.$label.' type=text >\r</p>';
        break;
    case "city":
        $label = "City";
         $inputFiled = '<input name=CITY placeholder='.$label.' type=text >\r</p>';
        break;
    case "country":
        $label = "Country";
         $inputFiled = '<input name=COUNTRY placeholder='.$label.' type=text >\r</p>';
        break;
    case "organization":
        $label = "Organization";
         $inputFiled = '<input name=ORGANIZATION placeholder='.$label.' type=text >\r</p>';
        break;
    case "salutation":
        $label = "Salutation";
         $inputFiled = '<select name=SALUTATION><option value=></option><option value=Mr>Mr</option><option value=Mrs>Mrs</option></select>\r</p>';
        break;
    case "zip":
        $label = "Zip";
         $inputFiled = '<input name=ZIP placeholder='.$label.' type=text >\r</p>';
        break;
    case "address":
        $label = "Street&nbsp;Address";
         $inputFiled = '<input name=ADDRESS placeholder='.$label.' type=text >\r</p>';
        break;
    case "sex":
        $label = "Sex";
         $inputFiled = '<select name=GENDER><option value=></option><option value=m>Mr</option><option value=f>Mrs</option></select>\r</p>';
        break;
    case "birthday":
        $label = "Birth Day";    
         $inputFiled = '<input name=BIRTHDAY  type=date >\r</p>';
        break;
    case "title":
        $label = "Title";
         $inputFiled = '<input name=TITLE placeholder='.$label.' type=text >\r</p>';
     case "upload":
        $label = "Upload";
        $inputFiled = '<input type=file name=my_image_upload[] id=my_image_upload[]  accept=image/png,image/jpeg,image/jpg,image/gif,image/tiff multiple=multiple />\r</p>';
        break;
     case "agree":
        $label = "";
        $inputFiled = '<input type=checkbox required name=terms >' . $termsJS.'\r</p>';
        break;    
    case "submit":
        $submit = '\r<p>\r<input type=submit value=Submit />\r</p>';
         break; 
    default:
        $label = $input;  
}

     $labelname = '\r<p> \r<label>'.$label.'</label> \r';
       $newtext = $labelname.$inputFiled;	
	
        if($input === 'submit'){
           $text  = $submit;
        }else{
            $text = $newtext;
        }
        
    return $text;        
}

?> 
<style>
.myButton {
	background-color:#7abe73;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	border:1px solid transparent;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:12px;
	padding:5px 10px;
	text-decoration:none;
	
}
.myButton:hover {
	background-color:#6f6f6f ;
}
.myButton:active {
	position:relative;
	top:1px;
}
  .form-table th {

    vertical-align: top;
    text-align: left;
    padding: 20px 10px 20px 0;
    width: 200px;
    line-height: 1.3;
    font-weight: 600;
    font-size: 14px;

}
label {

    color: #23282d;
    font-weight: 400;
    text-shadow: none;
    vertical-align: baseline;

}
  
input[type="text"], input[type="search"], input[type="date"],input[type="radio"], input[type="tel"], input[type="time"], input[type="url"], input[type="week"], input[type="password"], input[type="checkbox"], input[type="color"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="email"], input[type="month"], input[type="number"], select, textarea {

    border: 1px solid #ddd;
    box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
    background-color: #fff;
    color: #32373c;
    outline: 0;
    transition: 50ms border-color ease-in-out;
    padding: 3px 5px;

}
    input[type="checkbox"]{

    float: left;

}
    
.widefat {

    border-spacing: 0;
    width: 100%;
    clear: both;
    margin: 0;

}
    
.submit-btn{
        
    border:1px solid red;
        
    }
    
    /* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    clear: both;
}

/* Modal Content */
.modal-content {
    position: relative;
    clear: both;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 20%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
    border: 1px solid #ddd;
    box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
    background-color: #fff;
    color: #32373c;
    outline: 0;
    transition: 50ms border-color ease-in-out;
    padding: 3px 5px;
}
</style>

<style>
    
        .mio-modal-wrapper{
            
            
        }
        
        .mio-modal-body{
            
            background-color: #fff;
            
            padding: 25px;
        }
        
        .mio-modal-content-input{
            
            width:100%;
            padding-bottom: 10px;
            
        }
        
         .mio-modal-content-input input{
            
            width:300px;
            
        }
        
        .mio-modal-content-input select{
             
            width:300px;
            
        }
        
        .mio-close{
            
            color:#000;
        }
        
        .info{
            
            font-size: 10px;
            background-color: #f5f5f5;
        }
    
    .round {
  -webkit-border-top-left-radius: 1px;
  -webkit-border-top-right-radius: 2px;
  -webkit-border-bottom-right-radius: 3px;
  -webkit-border-bottom-left-radius: 4px;

  -moz-border-radius-topleft: 1px;
  -moz-border-radius-topright: 2px;
  -moz-border-radius-bottomright: 3px;
  -moz-border-radius-bottomleft: 4px;

  border-top-left-radius: 1px;
  border-top-right-radius: 2px;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 4px;
}
      
    </style>

<script language="javascript" type="text/javascript">
    
    function insertAtCaret(areaId,text) {
	var txtarea = document.getElementById(areaId);
	var scrollPos = txtarea.scrollTop;
	var strPos = 0;
	var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
		"ff" : (document.selection ? "ie" : false ) );
	if (br == "ie") { 
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart ('character', -txtarea.value.length);
		strPos = range.text.length;
	}
	else if (br == "ff") strPos = txtarea.selectionStart;
	
	var front = (txtarea.value).substring(0,strPos);  
	var back = (txtarea.value).substring(strPos,txtarea.value.length); 
	txtarea.value=front+text+back;
	strPos = strPos + text.length;
	if (br == "ie") { 
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart ('character', -txtarea.value.length);
		range.moveStart ('character', strPos);
		range.moveEnd ('character', 0);
		range.select();
	}
	else if (br == "ff") {
		txtarea.selectionStart = strPos;
		txtarea.selectionEnd = strPos;
		txtarea.focus();
	}
	txtarea.scrollTop = scrollPos;
}
    function insertCustomField(areaId) {
        var value = document.getElementById("mail-in-one-woocommerce[customField]").value;
        var res = value.split("-");
        var label = res[0];
        var type = res[1];
        var newtext = '';
        if(label !== null && label !== '') {
         switch(type){
              case "date":
              newtext =  '<input name='+label+' placeholder='+label+' type=date >\r</p>';
              break;
              case "boolean":
              newtext = '<select name='+label+'><option value=></option><option value=true>True</option><option value=false>False</option></select>\r</p>';
              break;
           default:
               newtext =  '<input name='+label+' placeholder='+label+' type=text >\r</p>';
         }
       
        
        var labelname = '\r<p> \r<label>'+label+'</label> \r';
        var text = labelname + newtext;	

	var txtarea = document.getElementById(areaId);
	var scrollPos = txtarea.scrollTop;
	var strPos = 0;
	var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
		"ff" : (document.selection ? "ie" : false ) );
	if (br == "ie") { 
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart ('character', -txtarea.value.length);
		strPos = range.text.length;
	}
	else if (br == "ff") strPos = txtarea.selectionStart;
	
	var front = (txtarea.value).substring(0,strPos);  
	var back = (txtarea.value).substring(strPos,txtarea.value.length); 
	txtarea.value=front+text+back;
	strPos = strPos + text.length;
	if (br == "ie") { 
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart ('character', -txtarea.value.length);
		range.moveStart ('character', strPos);
		range.moveEnd ('character', 0);
		range.select();
	}
	else if (br == "ff") {
		txtarea.selectionStart = strPos;
		txtarea.selectionEnd = strPos;
		txtarea.focus();
	}
	txtarea.scrollTop = scrollPos;
    }
}
    
    function getShortCode(){
      var shortCode = document.getElementById("<?php echo $this->plugin_name; ?>-showCode").value;  
      window.location = "?page=mail-in-one-woocommerce&tab=newsletter&id="+shortCode;
    }

</script>

<?php
$email = 'email';
$firstName = 'firstName';
$lastName = 'lastName';
$city = 'city';
$country = 'country';
$organization = 'organization';
$salutation= 'salutation';
$zip = 'zip';
$address = 'address';
$submit = 'submit';
$agree_to_terms ='agree';
$sex = 'sex';
$birthday ='birthday';
$title = 'title';
$upload = 'upload';
//customer Filed
$customersFileds = $handler->getCustomersFiled(); 
$customersFiledsArray = $customersFileds->custom_fields;

?>
<input type="hidden" name="mail_in_one_active_settings_tab"  value="newsletter"/>
<input type="hidden" name="<?php echo $this->plugin_name; ?>[shortCode]" id="<?php echo $this->plugin_name; ?>-shortCode" value="<?php echo $shortCode;?>" />

<h1 style="width:78%; padding-top:25px; padding-bottom:25px;">Mail In One Contact Form</h1>

 <strong>Short Code:&nbsp;&nbsp;</strong>
 
 <select id="<?php echo $this->plugin_name; ?>-showCode" name="<?php echo $this->plugin_name; ?>[showCode]" onchange="getShortCode()">
     <option value="" >--Please choose an Short Code--</option> 
      <?php foreach ($sowTable as $showRow) {
          if($showRow->status == 'Activ'){
          ?>    
     <option value="<?php echo $showRow->id ;?>"  <?php echo $showRow->id == $shortCode?  'selected'  : ''; ?>><?php echo $showRow->name;?></option> 
          <?php  }}?>
 </select>


<div style="padding-top:10px; padding-bottom:10px;" class="table">
    
           <strong>Form Fields:&nbsp;&nbsp;</strong><br><br> 
           <input type="button" class="myButton" value="Email Addess" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($email, $termsJS);?>')">        
           <input type="button" class="myButton" value="First Name" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($firstName, $termsJS);?>')">          
           <input type="button" class="myButton" value="Last Name" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($lastName, $termsJS);?>')">         
           <input type="button" class="myButton" value="Street Address" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($address, $termsJS);?>')">         
           <input type="button" class="myButton" value="City" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($city, $termsJS);?>')">        
           <input type="button" class="myButton" value="Sex" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($sex, $termsJS);?>')">          
           <input type="button" class="myButton" value="Birth Day" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($birthday, $termsJS);?>')">        
           <input type="button" class="myButton" value="Title" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($title, $termsJS);?>')">
           <input type="button" class="myButton" value="Upload" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($upload, $termsJS);?>')">   
           <input type="button" class="myButton" value="Country" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($country, $termsJS);?>')">        
           <input type="button" class="myButton" value="Organization" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($organization, $termsJS);?>')">        
           <input type="button" class="myButton" value="Salutation" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($salutation, $termsJS);?>')">        
           <input type="button" class="myButton" value="Zip" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($zip, $termsJS);?>')">        
           <input type="button" class="myButton" value="Agree To Terms" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($agree_to_terms, $termsJS);?>')">  
           <input type="button" class="myButton" value="Submit Button" onClick="insertAtCaret('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>','<?php echo addtext2($submit, $termsJS);?>')"> 
           <input type="button" class="myButton" id="myBtn" value="+ add Custom Field">   
           
</div>



<div style="margin-top:18px; float:left; width:60%;">
    <p><strong>Form Code:</strong></p>
     <textarea style="position:relative; overflow:hidden; background: white; heigth:400px; width:100%" rows="40" id="<?php echo $this->plugin_name; ?>-form_code-<?php echo $shortCode;?>" name="<?php echo $this->plugin_name; ?>[form_code-<?php echo $shortCode;?>]"><?php echo htmlspecialchars($options['form_code-'.$shortCode], ENT_QUOTES, get_option('blog_charset')); ?></textarea>
</div>


<div style="padding-left: 50px;margin-top: 30px; float:left; width:20%;">
        <form class="submit-btn">
              <?php submit_button('Save and preview form', 'primary','submit', TRUE); ?>
        </form>
    </div>
 
<div style="width:37%; float:right;"> 
    <p><strong>Form preview:</strong></p>
    <div style="background-color:#f5f5f5; padding:25px;" class="form-preview">
        <?php 
           echo $options['css-'.$shortCode];
           echo '<form action="' . esc_url($_SERVER['REQUEST_URI']) . '" method="post" class="'.$options['form_css-'.$shortCode].'"> ';
           echo $options['form_code-'.$shortCode]; 
           echo '</form>';
        ?>
    </div>
</div>


<div>
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content" style="border:1px solid transparent; border-radius: 5px; background-color:#fff; min-width:330px;">
      <div>
      <span style="border-radius:50%; background-color:#fff; color:#000; margin-top:-20px; margin-right:-20px; padding-bottom:3px; padding:10px;box-shadow: 1px 2px 4px rgba(0, 0, 0, .5);" class="close">&times;</span>
      </div>
    <div class="modal-body">
        
      <p style="color:#fff; background-color:#7abe73; padding:10px;"><strong>Add a Custom Input Field</strong></p>     
      <span style=" "class="info"><i>Please select a custom input field. The custom field is defined and created in your Mail-In-One account. The custom input field allows you to ask for custom data in your form e.g. "Company" or "Website".</i></span>

      <select multiple style="margin-top:10px; height:400px; width:100%;" id ="<?php echo $this->plugin_name; ?>[customField]" name="<?php echo $this->plugin_name; ?>[customField]">
          
          
          
             
                <?php foreach ($customersFiledsArray as $customerFiled => $customerType) {?>    
                <option value="<?php echo $customerFiled.'-'.$customerType; ?>"><?php echo $customerFiled;?></option>   
                <?php } ?>
          
          
          
     </select>
           
           
        
        
    </div>
    <div class="modal-footer" style="box-shadow:none; outline:none; border:none; padding:15px;">
        
        <input type="button" class="button button-primary" value="Add Custom Field" onClick="insertCustomField('mail-in-one-woocommerce-form_code-<?php echo $shortCode;?>')"> 
        &nbsp;
        <!-- <input type="button" class="button button-secondary close" value="cancel"> -->
        
    </div>
  </div>

</div>     
</div>


<script language="javascript" type="text/javascript">
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

</script>





    


