<?php

if (isset($options['mail_in_one_api_key']) && !$handler->hasValidApiKey()) {
    include_once __DIR__.'/errors/missing_api_key.php';
}
$campaigns = $handler->getCampaign();
?>
<table class="wp-list-table widefat fixed striped pages">
<thead>
  <tr>
    <th style="width: 50px">ID</th>
    <th>Name</th>
  </tr>
  </thead>
  <tbody>
<?php
foreach ($campaigns->getResult() as $campaign){
   ?>  
  <tr>
    <td style="width: 50px"><?php echo $campaign->id; ?></td>
    <td><?php echo $campaign->fields["name"];?></td>
  </tr>
    
<?php 
}
?> 
</tbody>
</table>

