
<?php
if($has_valid_api_key){
$doiMailings = $handler->getDOIMailing();
$contactEvents = $handler->getContactEvents();
$sowTable = $handler->showTable();
}
?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<style>
    
body{background-color: #f1f1f1;}
    
label {

    color: #23282d;
    font-weight: 400;
    text-shadow: none;
    vertical-align: baseline;

}
  
input[type="text"], input[type="search"], input[type="date"],input[type="radio"], input[type="tel"], input[type="time"], input[type="url"], input[type="week"], input[type="password"], input[type="checkbox"], input[type="color"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="email"], input[type="month"], input[type="number"], select, textarea {

    border: 1px solid #ddd;
    box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
    background-color: #fff;
    color: #32373c;
    outline: 0;
    transition: 50ms border-color ease-in-out;
    padding: 3px 5px;

}
    input[type="checkbox"]{

    float: left;

}
.myButton {
	background-color:#7abe73 ;
	-moz-border-radius:12px;
	-webkit-border-radius:12px;
	border-radius:12px;
	border:1px solid #7abe73;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:12px;
	padding:5px 10px;
	text-decoration:none;
	text-shadow:0px 1px 0px #7abe73;
    position:relative;
	top:1px;
}

.myButton2 {
	background-color:#A8A8A8;
	-moz-border-radius:12px;
	-webkit-border-radius:12px;
	border-radius:12px;
	border:1px solid #7abe73;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:12px;
	padding:5px 10px;
	text-decoration:none;
	text-shadow:0px 1px 0px #7abe73 ;
        position:relative;
	top:1px;
}

/* Dialog Form */
  /* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    clear: both;
    border:transparent!important;
}

/* Modal Content */
.modal-content {
    position: relative;
    clear: both;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 20%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}
    
.modal-footer {

}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.table-1{
    width: auto;   
    border: 2px solid white;
    border-radius: 15px;
}
.header-th {
    background-color: #4CAF50;
    color: white;
}
.header-td {
    border: 1px solid white;
}
    
.mio-modal-wrapper{}
        
.mio-modal-body{
            
  background-color: #fff;
            
            padding: 25px;
        }
        
        .mio-modal-content-input{
            
            width:100%;
            padding-bottom: 10px;
            
        }
        
         .mio-modal-content-input input{
            
            width:300px;
            
        }
        
        .mio-modal-content-input select{
             
            width:300px;
            
        }
        
        .mio-close{
            
            color:#fff; 
        }
        
        .info{
            
            font-size: 10px;
            background-color: #f5f5f5;
        }
    
    
    
.btn-success {
    background-color: #7abe73!important;
    border-color: #7abe73!important;
}
    
.modal-header{
        
    background-color:#7abe73!important;
    color:#fff!important;
}
      
</style>
 

<?php

if (isset($options['mail_in_one_api_key']) && !$handler->hasValidApiKey()) {
    include_once __DIR__.'/errors/missing_api_key.php';
}

?>
    <input type="hidden" name="mail_in_one_active_settings_tab" value="api_key"/>
        <td>
            <input type="hidden"  id="<?php echo $this->plugin_name; ?>-form_doi_key_mail_in_one" name="<?php echo $this->plugin_name; ?>[form_doi_key_mail_in_one]"    value="<?php echo isset($options['form_doi_key_mail_in_one']) ? $options['form_doi_key_mail_in_one'] : '' ?>"
        </td>
                    
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">
                        Status
                    </th>
                    <td>
                        <?php if( $has_valid_api_key ) { ?>
                        <span class="myButton" >CONNECTED</span>
                        <?php } else { ?>
                        <span class="myButton2">NOT CONNECTED</span>
                        <?php } ?>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">API Key</th>
                    <td>  
                        <input  style="width: 30%;" type="text" class="widefat" placeholder="Your Mail In One API key" id="<?php echo $this->plugin_name; ?>-mail_in_one_api_key3" name="<?php echo $this->plugin_name; ?>[mail_in_one_api_key]" value="<?php echo isset($options['mail_in_one_api_key']) ? $options['mail_in_one_api_key'] : '' ?>" />						
                    </td>
                </tr>                                      
            </table>
            <p>&nbsp;</p>
  
  <?php if($has_valid_api_key){?> 
                                     
  <table id="<?php echo $this->plugin_name; ?>-table_short_code" name="<?php echo $this->plugin_name; ?>[table_short_code]" class="table table-striped" >
     <thead class="thead-dark">
      <tr>
          <th scope="col">ID</th>
          <th scope="col">Short Code</th>
          <th scope="col">Name</th>
          <th scope="col">Type Of Integration</th>
          <th scope="col">DOI Name</th>
          <th scope="col">DOI Key</th>
          <th scope="col">Contact Event Name</th>
          <th scope="col">Contact Event Key</th>      
          <th scope="col">Marketing Automation</th>
          <th scope="col">Status</th>
          <th scope="col">Action</th>
      </tr> 
    </thead>
      <?php foreach ($sowTable as $showRow) {
     switch ($showRow->typeOfIntegration) {
         case 'NONE':
             $typeValue = '5';
             break;
         case 'DOI':
             $typeValue = '1';
             break;
         case 'Contact Event':
             $typeValue = '2';
             break;
         case 'Contact Event if DOI already exists':
             $typeValue = '3';
             break;
         case 'Marketing Automation':
             $typeValue = '4';
             break;

         default:
             break;
     }?>
      <tr>
          <td scope="row"><strong><?php echo $showRow->id;?></strong></td>
          <td scope="row"><?php echo $showRow->short_code.'"'.$showRow->id.'"]';?></td>
          <td scope="row"><?php echo $showRow->name;?></td>
          <td scope="row"><?php echo $showRow->typeOfIntegration;?></td>
          <td scope="row"><?php echo $showRow->doi_name;?></td>
          <td scope="row"><?php echo $showRow->doi_key;?></td>
          <td scope="row"><?php echo $showRow->contactEvent_name;?></td>
          <td scope="row"><?php echo $showRow->contactEvent_key;?></td>  
          <td scope="row"><?php if($showRow->marketing_automation == 0){echo ' ';}else{echo $showRow->marketing_automation;}?></td>
          <td scope="row"><?php echo $showRow->status;?></td>
          <td scope="row">
              
              <input type="button" class="btn btn-danger"  value="Delete" onclick="myActionDelete('<?php echo $showRow->name?>' , '<?php echo $showRow->id?>')"> 
              
              <input type="button" class="btn btn-primary"  value="Edit" onclick="myActionEdit('<?php echo $showRow->id?>' , '<?php echo $showRow->name?>', '<?php echo $showRow->doi_key?>', '<?php echo $showRow->doi_name?>','<?php echo $showRow->status?>','<?php echo $typeValue;?>','<?php echo $showRow->contactEvent_name;?>','<?php echo $showRow->contactEvent_key;?>','<?php echo $showRow->marketing_automation;?>')"> 
              
          </td>
      </tr>          
  <?php }?>
  </table>
      
<!-- MODAL: CREATE NEW FORM --> 
<div id="myModal" class="modal">
 <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create a new Form:</h5> 
        <span id="close" class="close mio-close">&times;</span>
      </div>

       <div class="modal-body">
            
        <div class="mio-modal-content-input">
            <label>Form Name:</label><br><input style="width:100;" type="text" id="<?php echo $this->plugin_name; ?>-tname" name="<?php echo $this->plugin_name; ?>[tname]"  /><br>
            <span class="info"><i>Please add the Name for your new Form e.g. "Newsletter Subscription Form"</i></span>
        </div>
            
        <div class="mio-modal-content-input">
        <label>Type Of Integration:</label><br>
            <select style="width:100;" id="<?php echo $this->plugin_name; ?>-typeOfIntegration" name="<?php echo $this->plugin_name; ?>[typeOfIntegration]" >  
                <option value="5">NONE</option>
                <option value="1">DOI</option>
                <option value="2">Contact Event</option>
                <option value="3">Contact Event if DOI already exists</option>
                <option value="4">Marketing Automation</option>                             
            </select>
            <br><span class="info"><i>Please select The Type Of Integration</i></span>
            </div>
            
        <div class="mio-modal-content-input">
            <label>DOI Mailing:</label><br>
            <select style="width:100;" id="<?php echo $this->plugin_name; ?>-tdoi" name="<?php echo $this->plugin_name; ?>[tdoi]" >
                      <option value="0" ></option> 
                    <?php             
                    foreach ($doiMailings as $doiMailing) { ?>
                     <option value="<?php echo $doiMailing['doiKey']. '__'. $doiMailing['name'];?>" ><?php echo $doiMailing['name'];?></option>            
                   <?php }
                    ?>           
                </select>
        <br><span class="info"><i>Please select an active DOI E-Mailing. DOI E-Mails are organised in<br> your Mail-In-One account.</i></span>
        </div>
            
        <div class="mio-modal-content-input">
            <label>Contact Events:</label><br>
            <select style="width:100;" id="<?php echo $this->plugin_name; ?>-contactEvents" name="<?php echo $this->plugin_name; ?>[contactEvents]" >
                <option value="0" ></option>
                    <?php
                    foreach ($contactEvents as $key => $contactEvent) { ?>
                     <option value="<?php echo $key. '__'. $contactEvent;?>" ><?php echo $contactEvent;?></option>            
                   <?php }
                    ?>           
            </select>
            <br><span class="info"><i>Please select an active Contact Event. Contact Events are organised in<br> your Mail-In-One account.</i></span>
            </div>
            
            <div class="mio-modal-content-input">
                <label>Marketing Automation:</label><br><input style="width:100;" type="number" id="<?php echo $this->plugin_name; ?>-marketingAutomation" name="<?php echo $this->plugin_name; ?>[marketingAutomation]"  /><br>
                <span class="info"><i>Please add the Number Of Marketing Automation</i></span>
            </div>
            
        <div style="border:transparent;" class="modal-footer">
            <?php submit_button('Create Form', 'primary','submit', TRUE); ?>    
        </div>
           
    </div>     
     
</div> 
</div> 
<!-- ./MODAL: CREATE NEW FORM -->  
      
  <div>&nbsp;</div>
  <input type="button" class="btn btn-success btn-lg" id="myBtn" value="+ Create New Form">  
  <?php } ?>
       
<!-- MODAL: DELETE --> 
<div id="myModal2" class="modal">
 <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to delete this form?</h5> 
         <span id="close2" class="close mio-close">&times;</span>  
      </div>

       <div class="modal-body">
         Form Name:&nbsp;<strong><input style="border:transparent;" type="text" name="<?php echo $this->plugin_name; ?>[cf_name]" id="<?php echo $this->plugin_name; ?>-cf_name" value="" readonly/></strong>
         <input type="hidden" name="<?php echo $this->plugin_name; ?>[cf_id]" id="<?php echo $this->plugin_name; ?>-cf_id" value="" />
         <input type="hidden" name="<?php echo $this->plugin_name; ?>[action]" id="<?php echo $this->plugin_name; ?>-action" value="delete" />
       </div>   
      <div style="border:transparent!important;" class="modal-footer">
         <?php submit_button('Delete Form', 'primary','submit', TRUE); ?> 
      </div>
    </div>     
</div> 
<!-- ./MODAL: DELETE -->       
      
<!-- MODAL: EDIT --> 
<div id="myModal3" class="modal">
 <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit your Form</h5> 
        <span id="close3" class="close mio-close">&times;</span>
      </div>

       <div class="modal-body">

         <input type="hidden" name="<?php echo $this->plugin_name; ?>[cf_id3]" id="<?php echo $this->plugin_name; ?>-cf_id3" value="" />
         <input type="hidden" name="<?php echo $this->plugin_name; ?>[action3]" id="<?php echo $this->plugin_name; ?>-action3" value="update" />
           
        <div class="form-group"> 
        <label><strong>Form Name:</strong></label><br>
        <input style="border:transparent; width:100%;" type="text" name="<?php echo $this->plugin_name; ?>[cf_name3]" id="<?php echo $this->plugin_name; ?>-cf_name3" value="" readonly/>
        </div>

        <div class="form-group"> 
        <label><strong>Type of integration</strong></label><br>
        <select style="width:100%;" id="<?php echo $this->plugin_name; ?>-typeOfIntegration3" name="<?php echo $this->plugin_name; ?>[typeOfIntegration3]" >
                 <option value="5">NONE</option>
                 <option value="1">DOI</option>
                <option value="2">Contact Event</option>
                <option value="3">Contact Event if DOI already exists</option>
                <option value="4">Marketing Automation</option>                     
        </select>
        </div>
             
        <div class="form-group"> 
        <label><strong>DOI Mailing</strong></label><br>     
        <select style="width:100%;" id="<?php echo $this->plugin_name; ?>-tdoi3" name="<?php echo $this->plugin_name; ?>[tdoi3]" >
                  <?php
                   foreach ($doiMailings as $doiMailing) { ?>
                    <option value="<?php echo $doiMailing['doiKey']. '__'. $doiMailing['name'];?>" ><?php echo $doiMailing['name'];?></option>            
                  <?php }
                ?>           
         </select>
         </div>
        
        <div class="form-group"> 
        <label><strong>Contact Event</strong></label><br>
        <select style="width:100%;" id="<?php echo $this->plugin_name; ?>-contactEvent3" name="<?php echo $this->plugin_name; ?>[contactEvent3]" >
                  <?php
                   foreach ($contactEvents as $key => $contactEvent) { ?>
                     <option value="<?php echo $key. '__'. $contactEvent;?>" ><?php echo $contactEvent;?></option>           
                  <?php }
                ?>           
        </select>
        </div>
                
        <div class="form-group"> 
        <label><strong>Marketing Automation</strong></label><br>
        <input style="width:100%;" type="number" name="<?php echo $this->plugin_name; ?>[marketing_automation3]" id="<?php echo $this->plugin_name; ?>-marketing_automation3" value="" />
        </div>
        
        <div class="form-group"> 
        <label><strong>Status</strong></label><br>
        <select style="width:100%;" id="<?php echo $this->plugin_name; ?>-cf_status3" name="<?php echo $this->plugin_name; ?>[cf_status3]" >
                <option value="Activ">active</option>
                <option value="Deactivate">deactivated</option>
            </select>
       </div>   
          
      <div class="modal-footer">
         <?php submit_button('Save form', 'primary','submit', TRUE); ?>
      </div>
    </div>     
     
</div> 
</div> 
<!-- ./MODAL: EDIT -->       
      
  

 
<script language="javascript" type="text/javascript">
    // Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

function myActionDelete(shortCode, id){
    var modal2 = document.getElementById('myModal2');
     modal2.style.display = "block";
     // Get the <span> element that closes the modal
    var span2 = document.getElementById("close2");
// When the user clicks on <span> (x), close the modal
   span2.onclick = function() {
    modal2.style.display = "none";
 }
    document.getElementById('mail-in-one-woocommerce-cf_name').value = shortCode;
    document.getElementById('mail-in-one-woocommerce-cf_id').value = id;
    
}

function myActionEdit(id3,shortCode3,doiKey, doiName, status,typeOfIntegration,contactEvent_name,contactEvent_key,marketing_automation){
    
    var modal3 = document.getElementById('myModal3');
     modal3.style.display = "block";
     // Get the <span> element that closes the modal
    var span3 = document.getElementById("close3");
// When the user clicks on <span> (x), close the modal
   span3.onclick = function() {
    modal3.style.display = "none";
 }
    document.getElementById('mail-in-one-woocommerce-cf_id3').value = id3;
    document.getElementById('mail-in-one-woocommerce-cf_name3').value = shortCode3;
    document.getElementById('mail-in-one-woocommerce-cf_status3').value = status;
    document.getElementById('mail-in-one-woocommerce-tdoi3').value = doiKey+'__'+doiName;
    document.getElementById('mail-in-one-woocommerce-typeOfIntegration3').value = typeOfIntegration;
    document.getElementById('mail-in-one-woocommerce-contactEvent3').value = contactEvent_key+'__'+contactEvent_name;
    document.getElementById('mail-in-one-woocommerce-marketing_automation3').value = marketing_automation;
}
</script>


   