<fieldset>
    <legend class="screen-reader-text">
        <span>Enable Debugging</span>
    </legend>
    <label for="<?php echo $this->plugin_name; ?>-mail_in_one-debugging">
        <select name="<?php echo $this->plugin_name; ?>[mail_in_one_debugging]" style="width:30%">

            <?php

            $enable_mail_in_one_debugging = (array_key_exists('mail_in_one_debugging', $options) && !is_null($options['mail_in_one_debugging'])) ? $options['mail_in_one_debugging'] : '1';

            foreach (array('0' => 'No', '1' => 'Yes') as $key => $value ) {
                echo '<option value="' . esc_attr($key) . '" ' . selected($key == $enable_mail_in_one_debugging, true, false ) . '>' . esc_html( $value ) . '</option>';
            }
            ?>

        </select>
        <span><?php esc_attr_e('Enable debugging logs to be sent to Mail In One.', $this->plugin_name); ?></span>
    </label>
</fieldset>
