<?php

if (isset($options['mail_in_one_api_key']) && !$handler->hasValidApiKey()) {
    include_once __DIR__.'/errors/missing_api_key.php';
}

$doiMailings = $handler->getDOIMailing();

?>
<table class="wp-list-table widefat fixed striped pages">
<thead>
<tr>

<th>Name</th>
<th>DOI Key</th>
<th></th>

</tr>
</thead>
<tbody>
<?php
foreach ($doiMailings as $doiMailing){
    ?>
  <tr>
   
    <td><?php echo $doiMailing['name']; ?></td>
    <td><?php echo $doiMailing['doiKey']; ?></td>
     <td><input type="radio" id="<?php echo $this->plugin_name; ?>-doi" name="<?php echo $this->plugin_name; ?>[doi]" value="<?php echo isset($options['doi']) ? $options['doi'] : '' ?>"><td>
  </tr>
    
<?php 
}
?> 
</tbody>
</table>
