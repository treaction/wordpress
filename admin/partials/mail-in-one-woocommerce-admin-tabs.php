<?php
$active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'api_key';
$is_mail_in_one_post = isset($_POST['mail-in-one_woocommerce_settings_hidden']) && $_POST['mail-in-one_woocommerce_settings_hidden'] === 'Y';

$handler = MailInOne_Woocommerce_Admin::connect();

//Grab all options for this particular tab we're viewing.
$options = get_option($this->plugin_name, array());

if (!isset($_GET['tab']) && isset($options['active_tab'])) {
    $active_tab = $options['active_tab'];
}

$show_sync_tab = isset($_GET['resync']) ? $_GET['resync'] === '1' : false;
$show_campaign_defaults = true;
$has_valid_api_key = false;
$allow_new_list = true;

$clicked_sync_button = $is_mail_in_one_post&& $active_tab == 'sync';

if (isset($options['mail_in_one_api_key']) && $handler->hasValidApiKey()) {
    $has_valid_api_key = true;
    // only display this button if the data is not syncing and we have a valid api key
    if ((bool) $this->getData('sync.started_at', false)) {
        $show_sync_tab = true;
    }
}

?>

<style>
    #sync-status-message strong {
        font-weight:inherit;
    }
    #log-viewer {
        background: #fff;
        border: 1px solid #e5e5e5;
        box-shadow: 0 1px 1px rgba(0,0,0,.04);
        padding: 5px 20px;
    }
    #log-viewer-select {
        padding: 10px 0 8px;
        line-height: 28px;
    }
    #log-viewer pre {
        font-family: monospace;
        white-space: pre-wrap;
    }
    user agent stylesheet
    pre, xmp, plaintext, listing {
        display: block;
        font-family: monospace;
        white-space: pre;
        margin: 1em 0px;
    }
</style>

<?php if (!defined('PHP_VERSION_ID') || (PHP_VERSION_ID < 70000)): ?>
    <div data-dismissible="notice-php-version" class="error notice notice-error is-dismissible">
        <p><?php _e('Mail In One says: Please upgrade your PHP version to a minimum of 7.0', 'mail-in-one-woocommerce'); ?></p>
    </div>
<?php endif; ?>

<!-- Create a header in the default WordPress 'wrap' container -->
<div class="wrap">
    <div id="icon-themes" class="icon32"></div>
    <h2>Mail In One Plugin</h2>

    <h2 class="nav-tab-wrapper">
        <a href="?page=mail-in-one-woocommerce&tab=api_key" class="nav-tab <?php echo $active_tab == 'api_key' ? 'nav-tab-active' : ''; ?>">General</a>
        <a href="?page=mail-in-one-woocommerce&tab=store_info" class="nav-tab <?php echo $active_tab == 'store_info' ? 'nav-tab-active' : ''; ?>">Get API Key</a>
        <?php if($has_valid_api_key): ?>
        <?php if($show_sync_tab): ?>
        <a href="?page=mail-in-one-woocommerce&tab=email_notification" class="nav-tab <?php echo $active_tab == 'email_notification' ? 'nav-tab-active' : ''; ?>">Email Notification</a>
        <a href="?page=mail-in-one-woocommerce&tab=sync" class="nav-tab <?php echo $active_tab == 'sync' ? 'nav-tab-active' : ''; ?>">Synchronize All</a>
       <!-- <a href="?page=mail-in-one-woocommerce&tab=doiMailing" class="nav-tab <?php #echo $active_tab == 'doiMailing' ? 'nav-tab-active' : ''; ?>">DOI Mailing</a>-->
        <a href="?page=mail-in-one-woocommerce&tab=newsletter" class="nav-tab <?php echo $active_tab == 'newsletter' ? 'nav-tab-active' : ''; ?>">Form Builder</a>
        <a href="?page=mail-in-one-woocommerce&tab=messages" class="nav-tab <?php echo $active_tab == 'messages' ? 'nav-tab-active' : ''; ?>">Content</a>    
        <a href="?page=mail-in-one-woocommerce&tab=styling" class="nav-tab <?php echo $active_tab == 'styling ' ? 'nav-tab-active' : ''; ?>">Styling </a>
       <!-- <a href="?page=mail-in-one-woocommerce&tab=targetGroup" class="nav-tab <?php #echo $active_tab == 'targetGroup' ? 'nav-tab-active' : ''; ?>">Verteiler</a>
        <a href="?page=mail-in-one-woocommerce&tab=contactfilter" class="nav-tab <?php #echo $active_tab == 'contactfilter' ? 'nav-tab-active' : ''; ?>">Kontaktfilter</a>-->
        <?php endif;?>
        <?php endif; ?>
    </h2>
 
    <form method="post" name="cleanup_options" action="options.php">

        <input type="hidden" name="mail-in-one_woocommerce_settings_hidden" value="Y">

        <?php
        if (!$clicked_sync_button) {
            settings_fields($this->plugin_name);
            do_settings_sections($this->plugin_name);
            include('tabs/notices.php');
        }
        ?>

        <input type="hidden" name="<?php echo $this->plugin_name; ?>[mailInOne_active_tab]" value="<?php echo $active_tab; ?>"/>
        

        <?php if ($active_tab == 'api_key' ): ?>
            <?php include_once 'tabs/api_key.php'; ?>
        <?php endif; ?>
        
         <?php if ($active_tab == 'store_info' ): ?>
            <?php include_once 'tabs/store_info.php'; ?>
        <?php endif; ?>
        
        <?php if ($active_tab == 'email_notification' ): ?>
            <?php include_once 'tabs/email_notification.php'; ?>
        <?php endif; ?>
  
        <?php if ($active_tab == 'sync' && $show_sync_tab): ?>
            <?php include_once 'tabs/store_sync.php'; ?>
        <?php endif; ?>

        <?php if ($active_tab == 'messages' && $show_sync_tab): ?>
            <?php include_once 'tabs/messages.php'; ?>
        <?php endif; ?>
        
        <?php if ($active_tab == 'targetGroup' && $show_sync_tab): ?>
            <?php #include_once 'tabs/targetGroup.php'; ?>
        <?php endif; ?>
        
        <?php if ($active_tab == 'contactfilter' && $show_sync_tab): ?>
            <?php #include_once 'tabs/contactfilter.php'; ?>
        <?php endif; ?>
        
        <?php if ($active_tab == 'doiMailing' && $show_sync_tab): ?>
            <?php #include_once 'tabs/doiMailing.php'; ?>
        <?php endif; ?>
           
         <?php if ($active_tab == 'newsletter' && $show_sync_tab): ?>
            <?php include_once 'tabs/newsletter.php'; ?>
        <?php endif; ?>
        
         <?php if ($active_tab == 'styling' && $show_sync_tab): ?>
            <?php include_once 'tabs/styling.php'; ?>
        <?php endif; ?>
        
        <?php #if ($active_tab !== 'sync' && $active_tab !== 'logs' && $active_tab !== 'store_info') submit_button('Save all changes', 'primary','submit', TRUE); ?>
        
        
       <?php if($active_tab == 'newsletter'){
             
        }elseif($active_tab !== 'sync' && $active_tab !== 'logs' && $active_tab !== 'store_info' && $active_tab !== 'email_notification'){
            submit_button('Save all changes', 'primary','submit', TRUE);
             ?></form> <?php
        }
?>

    


</div><!-- /.wrap -->
