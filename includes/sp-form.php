<?php

function show() {
    global $wpdb;
    return $wpdb->get_results("SELECT * FROM {$wpdb->prefix}mio_short_form");
}

$options = get_option('mail-in-one-woocommerce');

function html_form_code($options, $showRow) {
    echo $options['css-' . $showRow->id];
    echo '<form id="featured_upload" action="' . esc_url($_SERVER['REQUEST_URI']) . '" method="post" class="' . $options['form_css-' . $showRow->id] . '" enctype="multipart/form-data"> ';
    echo $options['form_code-' . $showRow->id];
    ?>

    <input type="hidden" name="post_id" id="post_id" value="55" />
    <?php wp_nonce_field('my_image_upload', 'my_image_upload_nonce'); ?>  
    <?php

    echo '</form>';
}

// This method creates/updates a contact with the given id in Maileon
function createContact($email, $FIRSTNAME, $LASTNAME, $gender, $config) {

    $debug = FALSE;
    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug($debug);

    $newContact = new com_maileon_api_contacts_Contact();
    $newContact->email = $email;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$FIRSTNAME] = $FIRSTNAME;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$LASTNAME] = $LASTNAME;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$SALUTATION] = $gender;
    $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;

    $response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "", "", false);

    return $response;
}

// check Contact
function existsContact($email, $config) {

    $debug = FALSE;
    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug($debug);
    $response = $contactsService->getContactByEmail($email);

    return $response->isSuccess();
}

function newsletter($options, $showRow) {

    $standard_fields = array(
        'ADDRESS',
        'BIRTHDAY',
        'CITY',
        'COUNTRY',
        'FIRSTNAME',
        'GENDER',
        'HNR',
        'LASTNAME',
        'ORGANIZATION',
        'REGION',
        'SALUTATION',
        'ZIP',
        'STATE',
        'TITLE'
    );
    $is_not_custom_fields = array(
        'post_id',
        'my_image_upload',
        '_wp_http_referer',
        'my_image_upload_nonce'
    );
    if ($_POST) {
        $config = array(
            'BASE_URI' => 'https://api.maileon.com/1.0',
            'API_KEY' => $options['mail_in_one_api_key'],
            'THROW_EXCEPTION' => true,
            'TIMEOUT' => 60,
            'DEBUG' => 'false' // NEVER enable on production
        );

        $contactsService = new com_maileon_api_contacts_ContactsService($config);
        $contactsService->setDebug(false);

        $transactionsService = new com_maileon_api_transactions_TransactionsService($config);
        $transactionsService->setDebug(false);

        $newContact = new com_maileon_api_contacts_Contact();
        $newContact->email = $_POST['email'];
        $newContact->anonymous = false;
        $newContact->permission = com_maileon_api_contacts_Permission::$NONE;

        foreach ($_POST as $key => $value) {
            ${$key} = $value;
            if (in_array($key, $standard_fields)) {
                if ($key == 'BIRTHDAY') {
                    $value = date('d.m.Y', strtotime($value));
                }
                $newContact->standard_fields[$key] = $value;
            } elseif ((!in_array($key, $is_not_custom_fields)) && $key != 'email' && $key != 'terms') {
                $newContact->custom_fields[$key] = $value; // 
            }
        }
        switch ($showRow->typeOfIntegration) {
            case 'NONE':
                $result = true;
                break;
            case 'DOI':
                $response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', true, true, (string) $showRow->doi_key);
                $result = $response->isSuccess();
                break;
            case 'Contact Event':
                createContact($_POST['email'], $_POST['FIRSTNAME'], $_POST['LASTNAME'], $_POST['SALUTATION'], $config);
                $transaction = new com_maileon_api_transactions_Transaction();
                $transaction->contact = new com_maileon_api_transactions_ContactReference();

                $transaction->contact->email = $_POST['email'];
                $transaction->type = (int) $showRow->contactEvent_key;
                $transaction->content['SALUTATION'] = $_POST['SALUTATION'];
                $transaction->content['FIRSTNAME'] = $_POST['FIRSTNAME'];
                $transaction->content['LASTNAME'] = $_POST['LASTNAME'];
                $transactions = array($transaction);
                $response = $transactionsService->createTransactions($transactions, false, false);
                $result = $response->isSuccess();
                break;
            case 'Contact Event if DOI already exists':
                //Check Conatct
                $contactExisted = existsContact($_POST['email'], $config);
                if ($contactExisted) {
                    #createContact($_POST['email'], $_POST['FIRSTNAME'], $_POST['LASTNAME'], $_POST['SALUTATION'], $config);
                    $transaction = new com_maileon_api_transactions_Transaction();
                    $transaction->contact = new com_maileon_api_transactions_ContactReference();

                    $transaction->contact->email = $_POST['email'];
                    $transaction->type = (int) $showRow->contactEvent_key;
                    $transaction->content['SALUTATION'] = $_POST['SALUTATION'];
                    $transaction->content['FIRSTNAME'] = $_POST['FIRSTNAME'];
                    $transaction->content['LASTNAME'] = $_POST['LASTNAME'];
                    $transactions = array($transaction);
                    $response = $transactionsService->createTransactions($transactions, false, false);
                    $result = $response->isSuccess();
                } else {
                    $response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', true, true, (string) $showRow->doi_key);
                    $result = $response->isSuccess();
                }
                break;
            case 'Marketing Automation':
                if (!empty($showRow->marketing_automation)) {
                    // Important: the ID must be a number (and thus, submitted without "" to the REST-Service, otherwhile it will result in 400 - Bad Request)
                    $programId = intval(trim($showRow->marketing_automation));
                    $maService = new com_maileon_api_marketingAutomation_MarketingAutomationService($config);
                    $debug = false;
                    $maService->setDebug($debug);
                    $response = $maService->startMarketingAutomationProgram($programId, $_POST['email']);
                    $result = $response->isSuccess();
                }
                break;
        }

        if ($result) {
            if ($options['notification_email_notification'] == 'True') {

                $to = $options['notification_email_notification_list'];
                $subject = $options['notification_email_subject'];

                $messageBody = 'New Contact'
                        . \chr(13)
                        . '========================================' . \chr(13);
                foreach ($_POST as $key => $value) {
                    if (!in_array($key, $is_not_custom_fields)) {
                        $messageBody .= $key . ': ' . $value . \chr(13);
                    }
                }
                $attachment_id = 0;
                $attachments_ids = array();
                $server = $options['notification_email_server'];
                //Settings 
                $max_allowed_file_size = 1; // size in MB 
                $allowed_extensions = array("image/jpg", "image/jpeg", "image/png");

                // Check that the nonce is valid, and the user can edit this post.
                if (
                        isset($_POST['my_image_upload_nonce'], $_POST['post_id']) && wp_verify_nonce($_POST['my_image_upload_nonce'], 'my_image_upload')
                //&& current_user_can( 'edit_post', $_POST['post_id'] )
                ) {

                    // The nonce was valid and the user has the capabilities, it is safe to continue.
                    // These files need to be included as dependencies when on the front end.
                    require_once( ABSPATH . 'wp-admin/includes/image.php' );
                    require_once( ABSPATH . 'wp-admin/includes/file.php' );
                    require_once( ABSPATH . 'wp-admin/includes/media.php' );

                    // Let WordPress handle the upload.
                    // Remember, 'my_image_upload' is the name of our file input in our form above.

                    $files = $_FILES["my_image_upload"];

                    foreach ($files['name'] as $key => $value) {
                        if ($files['name'][$key]) {
                            $file = array(
                                'name' => $files['name'][$key],
                                'type' => $files['type'][$key],
                                'tmp_name' => $files['tmp_name'][$key],
                                'error' => $files['error'][$key],
                                'size' => number_format($files['size'][$key] / 1048576, 2) // size in MB
                            );
                            $_FILES = array("upload_file" => $file);
                            //------ Validate the file extension -----
                            $allowed_ext = false;   
                            $allowed_img = true;
                            
                              if (exif_imagetype($file['name'])){
                                  $allowed_img = false;
                              }
                                  
                            for ($i = 0; $i < sizeof($allowed_extensions); $i++) {
                                if (strcasecmp($allowed_extensions[$i], $file['type']) == 0) {
                                    $allowed_ext = true;
                                }
                            }
                            $file_size = true;
                            if ($file['size'] > $max_allowed_file_size) {
                                $file_size = false;
                            }
                            if (!$allowed_ext || !$file_size) {

                                echo!$file_size ? "\n Size of file " . $file['name'] . " should be less than $max_allowed_file_size" . "MB" : '';
                                echo "<br />";
                                echo!$allowed_ext ? "\n The uploaded file " . $file['name'] . " is not supported file type. " . " Only the following file types are supported: " . implode(',', $allowed_extensions) : '';
                            } else {
                                $attachment_id = media_handle_upload("upload_file", $_POST['post_id']);
                                // There was an error uploading the image.
                                echo is_wp_error($attachment_id) ? $attachment_id->get_error_message() : '';

                                array_push($attachments_ids, $attachment_id);
                            }
                        }
                    }
                }
                $headers = '';

                switch ($server) {

                    case 'WP-Server':
                        if (count($attachments_ids) > 0) {
                            foreach ($attachments_ids as $key => $attachment_id) {
                                $mail_attachments_wp[] = get_attached_file($attachment_id);
                            }
                            wp_mail(
                                    $to, $subject, $messageBody, $headers, $mail_attachments_wp
                            );
                            //delete attachment
                            foreach ($attachments_ids as $key => $attachment_id) {
                                wp_delete_attachment($attachment_id);
                            }
                        } else {
                            wp_mail(
                                    $to, $subject, $messageBody
                            );
                        }




                        break;
                    case 'SMTP-Server':

                        if (!empty($options['notification_email_serverName']) && !empty($options['notification_email_smtpHost']) &&
                                !empty($options['notification_email_smtpUser']) && !empty($options['notification_email_smtpPassword']) &&
                                !empty($options['notification_email_smtpEncryption']) && !empty($options['notification_email_smtpPort']) &&
                                !empty($options['notification_email_fromEmail']) && !empty($options['notification_email_fromName'])) {

                            $mailEngine = new SwiftMailer();
                            $mailEngine->setSmtpHost($options['notification_email_smtpHost']);
                            $mailEngine->setSmtpUsername($options['notification_email_smtpUser']);
                            $mailEngine->setSmtpPassword($options['notification_email_smtpPassword']);
                            $mailEngine->setSmtpPort($options['notification_email_smtpPort']);
                            if ($options['notification_email_smtpEncryption'] == '1') {
                                $mailEngine->setEncryption('tls');
                            }
                            $mailEngine->setServerName($options['notification_email_serverName']);
                            $mailEngine->setFromEmail($options['notification_email_fromEmail']);
                            $mailEngine->setFromName($options['notification_email_fromName']);
                            $mailEngine->init();

                            $Swift_Message = $mailEngine->createMessageObject();
                            /* @var $swiftMessage \Swift_Message */

                            $Swift_Message->setBody($messageBody)
                                    ->setSubject($subject);

                            $sendTo = explode(',', $options['notification_email_notification_list']);
                            $Swift_Message->setTo($sendTo);

                            if (count($attachments_ids) > 0) {
                                foreach ($attachments_ids as $key => $attachment_id) {
                                    $mail_attachment_smtp = get_attached_file($attachment_id);
                                    $Swift_Message->attach(Swift_Attachment::fromPath($mail_attachment_smtp));
                                }
                                $mailerSendResults = $mailEngine->sendMessage($Swift_Message);
                                //delete attachment
                                foreach ($attachments_ids as $key => $attachment_id) {
                                    wp_delete_attachment($attachment_id);
                                }
                            } else {
                                $mailerSendResults = $mailEngine->sendMessage($Swift_Message);
                            }
                        }

                        break;
                    default:
                        break;
                }
            }
            if (isset($options['success_url-' . $showRow->id]) && !empty($options['success_url-' . $showRow->id])) {
                wp_redirect(urldecode($options['success_url-' . $showRow->id]));
                exit();
            } else {
                echo '<div>';
                echo '<p><strong>' . $options['success-' . $showRow->id] . '</strong></p>';
                echo '</div>';
            }
        } else {
            echo '<div>';
            echo '<p><strong>' . $options['error-' . $showRow->id] . '</strong> </p>';
            var_dump($response);
            echo '</div>';
        }
    }
}

function mio_shortcode($id) {
    $showTable = show();
    extract(shortcode_atts(array(
        'id' => 'id'
                    ), $id));
    foreach ($showTable as $showRow) {
        if ($showRow->status == 'Activ' && $showRow->id == $id) {
            $options = get_option('mail-in-one-woocommerce');
            ob_start();
            newsletter($options, $showRow);
            html_form_code($options, $showRow);
        }
    }
    return ob_get_clean();
}

add_shortcode('mio_cf_form', 'mio_shortcode');
