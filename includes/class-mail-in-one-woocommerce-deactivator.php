<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://treaction.de
 * @since      1.0.0
 *
 * @package    MailInOne_Woocommerce
 * @subpackage MailInOne_Woocommerce/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    MailInOne_Woocommerce
 * @subpackage MailInOne_Woocommerce/includes
 * @author     Sami Jarmoud <sami.jarmoud@treaction.de>
 */
class MailInOne_Woocommerce_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		global $wpdb;

                $sql = "DROP TABLE IF EXISTS {$wpdb->prefix}mio_short_form;";
                $wpdb->query($sql);
               
	}

}
