<?php

use Automattic\WooCommerce\Client;

/**
 * User: Sami Jarmoud
 * Email: sami.jarmoud@treaction.de
 */
class MailInOne_Woocommerce_MailInOneApi {

    protected $version = '1.2';
    protected $api_key = null;
    protected $config = array();

    /**
     * MailInOne_Woocommerce_MailInOneApi constructor.
     * 
     * @param null $api_key
     */
    public function __construct($api_key = null) {
        if (!empty($api_key)) {
            $this->setApiKey($api_key);
        }
    }

    /**
     *
     * @param
     *            $key
     * @return $this
     */
    public function setApiKey($key) {
        $this->api_key = $key;

        return $this;
    }

    public function getConfing() {
        return $this->config = array(
            "BASE_URI" => "https://api.maileon.com/1.0",
            "API_KEY" => $this->api_key,
            "PROXY_HOST" => "",
            "PROXY_PORT" => "",
            "THROW_EXCEPTION" => true,
            "TIMEOUT" => 300,
            "DEBUG" => "false"
        );
    }

    /**
     *
     * @param
     *            $version
     * @return $this
     */
    public function setVersion($version) {
        $this->version = $version;

        return $this;
    }

    /**
     *
     * @param bool $return_profile
     * @return array|bool
     */
    public function ping($return_profile = false) {
        try {
            $profile = $this->getPing();
            return $return_profile ? $profile : true;
        } catch (MailInOne_WooCommerce_Error $e) {
            return false;
        }
    }

    /**
     *
     * @return array
     */
    public function woocommerceCustomers() {
        $site_url = get_site_url();
        $strip_settings = $this->getStripeSettings();
        // TODO Prod Key

        return $this->getWoocommerceCustomers($site_url, $strip_settings['test_publishable_key'], $strip_settings['test_secret_key']);
    }

    public function sync() {
        try {
            global $wpdb;
            $users = get_users($args);
            $customers = array();
            foreach ($users as $user) {
                $display_name = $user->data->display_name;
                $teile = explode(" ", $display_name);

                $customer['email'] = $user->data->user_email;
                $customer['first_name'] = $teile[0];
                $customer['last_name'] = $teile[1];
                $customer['role'] = $user->roles [0];
                $customer['user_registered'] = $user->data->user_registered;

                array_push($customers, $customer);
            }
            foreach ($customers as $customer) {
                $query_email = "SELECT email FROM `" . $wpdb->prefix . "mio_subscriber` WHERE email = %s";
                $results_email = $wpdb->get_results($wpdb->prepare($query_email, $customer['email']));
                if (is_null($results_email[0]->email)) {
                    $insert_user = "INSERT INTO `" . $wpdb->prefix . "mio_subscriber`( salutation, first_name, last_name, email, street_address, postal_code, city, country, telephone, organization, role, status, user_registered, created_at) 
                                                            VALUES ( '', '" . $customer['first_name'] . "', '" . $customer['last_name'] . "', '" . $customer['email'] . "', '', '', '', '', '', '', '" . $customer['role'] . "', 'new', '" . $customer['user_registered'] . "', '" . date("Y-m-d H:i:s") . "' )";
                    $wpdb->query($insert_user);
                }
            }
            return $this->syncCustomers($customers);
        } catch (\Exception $e) {
            throw new MailInOne_WooCommerce_Error($e);
        }
    }

    public function cronSync() {
        try {
            global $wpdb;
            $users = get_users($args);
            $customers = array();


            foreach ($users as $user) {
                $display_name = $user->data->display_name;
                $teile = explode(" ", $display_name);

                $customer['email'] = $user->data->user_email;
                $customer['first_name'] = $teile[0];
                $customer['last_name'] = $teile[1];
                $customer['role'] = $user->roles [0];
                $customer['user_registered'] = $user->data->user_registered;

                array_push($customers, $customer);
            }
            foreach ($customers as $customer) {
                $query_email = "SELECT email FROM `" . $wpdb->prefix . "mio_subscriber` WHERE email = %s";
                $results_email = $wpdb->get_results($wpdb->prepare($query_email, $customer['email']));
                if (is_null($results_email[0]->email)) {
                    $insert_user = "INSERT INTO `" . $wpdb->prefix . "mio_subscriber`( salutation, first_name, last_name, email, street_address, postal_code, city, country, telephone, organization, role, status, user_registered, created_at) 
                                                            VALUES ( '', '" . $customer['first_name'] . "', '" . $customer['last_name'] . "', '" . $customer['email'] . "', '', '', '', '', '', '', '" . $customer['role'] . "', 'new', '" . $customer['user_registered'] . "', '" . date("Y-m-d H:i:s") . "' )";
                    $wpdb->query($insert_user);
                }
            }

            $result = $this->cronSyncCustomers($customers);

            return $result;
        } catch (\Exception $e) {
            throw new MailInOne_WooCommerce_Error($e);
        }
    }

    public function getCampaign() {
        return $this->campaign();
    }

    public function getContactFilter() {
        return $this->contactFilter();
    }

    public function getTargetGroup() {
        return $this->targetGroup();
    }

    
    public function getDOIMailing() {
        return $this->DOIMailing();
    }
    
    public function getContactEvents() {
        return $this->contactEvents();
    }
    public function getCustomersFiled() {
        return $this->customersFiled();
    }

    /**
     *
     * @return array
     */
    protected function getStripeSettings() {
        // TODO Production Key
        return get_option('woocommerce_stripe_settings', false);
    }

    /**
     *
     * @return array|bool
     * @throws MailInOne_WooCommerce_Error
     */
    protected function getPing() {
        $pingService = new com_maileon_api_utils_PingService($this->getConfing());
        $debug = false;
        $pingService->setDebug($debug);

        $response = $pingService->pingGet();

        if (!$response->isSuccess()) {
            return false;
        } else {
            return array(
                'account_id' => '123456789',
                'username' => ''
            );
        }
    }

    protected function getWoocommerceCustomers($url, $publishable_key, $secret_key) {
        $wcClient = new Client($url, $publishable_key, $secret_key, [
            'wp_api' => true,
            'version' => 'wc/v2',
            'query_string_auth' => true
        ]);

        return $wcClient->get('customers');
    }

    /**
     *
     * @param array $customers
     * @throws MailInOne_WooCommerce_Error
     * @return em
     */
    protected function syncCustomers($customers) {
        // TODO Test mit falsche Config
        // TODO Fehlermeldung
        try {
            global $wpdb;
            $contacts = 0;
            $contactsService = new com_maileon_api_contacts_ContactsService($this->getConfing());
            $debug = FALSE;
            $contactsService->setDebug($debug);

            $contactsService->createCustomField('role', 'string');

            foreach ($customers as $customer) {
                $permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
                $useExternalId = false;
                $ignoreInvalidContacts = true;
                $reimportUnsubscribedContacts = false;
                $overridePermission = false;
                $updateOnly = false;
                $contactsToSync = new com_maileon_api_contacts_Contacts();
                $contactsToSync->addContact(new com_maileon_api_contacts_Contact(
                        '', $customer['email'], null, '', $anonymous = null, array(
                    'FIRSTNAME' => $customer['first_name'],
                    'LASTNAME' => $customer['last_name']
                        ), array(
                    'role' => $customer['role']
                        )
                ));
                $response = $contactsService->synchronizeContacts(
                        $contactsToSync, $permission, com_maileon_api_contacts_SynchronizationMode::$UPDATE, $useExternalId, $ignoreInvalidContacts, $reimportUnsubscribedContacts, $overridePermission, $updateOnly);

                if ($response->isSuccess()) {                   
                    $query_update = "UPDATE `".$wpdb->prefix."mio_subscriber`
                    					 SET status = %s
                    					 WHERE email      = %s ";
    		     $wpdb->query( $wpdb->prepare( $query_update,'synchronised', $customer['email']) );
                    $contacts ++;
                }
            }
            update_option('mail-in-one-woocommerce-resource-last-updated-contacts', 0);
            update_option('mail-in-one-woocommerce-resource-last-updated-contacts', $contacts);
            update_option('mail-in-one-woocommerce-resource-last-updated', time());
            return $response;
        } catch (\Exception $e) {
            throw new MailInOne_WooCommerce_Error($e);
        }
    }

    /**
     *
     * @param array $customers
     * @throws MailInOne_WooCommerce_Error
     * @return em
     */
    protected function cronSyncCustomers($customers) {
        // TODO Test mit falsche Config
        // TODO Fehlermeldung
        try {
            $dateNow = new DateTime('now');
            $dateNow->sub(new DateInterval('PT60M'));
            $dateTime = $dateNow->format('Y-m-d H:i:s');
            global $wpdb;
            $contacts = 0;
            $contactsService = new com_maileon_api_contacts_ContactsService($this->getConfing());
            $debug = FALSE;
            $contactsService->setDebug($debug);

            $contactsService->createCustomField('role', 'string');
            foreach ($customers as $customer) {
                if ($dateTime < $customer['user_registered']) {
                    $permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
                    $useExternalId = false;
                    $ignoreInvalidContacts = true;
                    $reimportUnsubscribedContacts = false;
                    $overridePermission = false;
                    $updateOnly = false;
                    $contactsToSync = new com_maileon_api_contacts_Contacts();
                    $contactsToSync->addContact(new com_maileon_api_contacts_Contact(
                            '', $customer['email'], null, '', $anonymous = null, array(
                        'FIRSTNAME' => $customer['first_name'],
                        'LASTNAME' => $customer['last_name'],
                            ), array(
                        'role' => $customer['role']
                            )
                    ));
                    $response = $contactsService->synchronizeContacts(
                            $contactsToSync, $permission, com_maileon_api_contacts_SynchronizationMode::$UPDATE, $useExternalId, $ignoreInvalidContacts, $reimportUnsubscribedContacts, $overridePermission, $updateOnly);

                    if ($response->isSuccess()) {
                        $query_update = "UPDATE `".$wpdb->prefix."mio_subscriber`
                    					 SET status = %s
                    					 WHERE email      = %s ";
    		     $wpdb->query( $wpdb->prepare( $query_update,'synchronised', $customer['email']) );
                        $contacts ++;
                    }
                }
            }
            update_option('mail-in-one-woocommerce-resource-last-updated-contacts', 0);
            update_option('mail-in-one-woocommerce-resource-last-updated-contacts', $contacts);
            update_option('mail-in-one-woocommerce-resource-last-updated', time());
            return $response;
        } catch (\Exception $e) {
            throw new MailInOne_WooCommerce_Error($e);
        }
    }

    protected function campaign() {
        try {
            $mailingService = new com_maileon_api_mailings_MailingsService($this->getConfing());
            $debug = false;
            $mailingService->setDebug($debug);

            $now = date('Y-m-d+');

            $fields = array(
                com_maileon_api_mailings_MailingFields::$NAME
            );

            $response = $mailingService->getMailingsBySchedulingTime($now . "00:00:00", false, $fields, 1, 10);
            return $response;
        } catch (\Exception $e) {
            throw new MailInOne_WooCommerce_Error($e);
        }
    }

    protected function contactFilter() {
        try {
            $contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($this->getConfing());
            $debug = false;
            $contactfiltersService->setDebug($debug);

            $response = $contactfiltersService->getContactFilters();
            return $response;
        } catch (\Exception $e) {
            throw new MailInOne_WooCommerce_Error($e);
        }
    }

    protected function targetGroup() {
        try {
            $targetGroupsService = new com_maileon_api_targetgroups_TargetGroupsService($this->getConfing());
            $debug = false;
            $targetGroupsService->setDebug($debug);

            $response = $targetGroupsService->getTargetGroups(1, 10);

            return $response;
        } catch (\Exception $e) {
            throw new MailInOne_WooCommerce_Error($e);
        }
    }

    protected function DOIMailing() {
        try {
            $mailingService = new com_maileon_api_mailings_MailingsService($this->getConfing());
            $debug = false;
            $mailingService->setDebug($debug);

            $mailings = $mailingService->getMailingsByTypes('doi',array('state'),1,1000);
            $doiMailingsArray = array();
            foreach ($mailings->getResult() as $mailing) {
				if($mailing->fields['state'] == 'released'){
                                    $id =$mailing->id;
                                    $doiMailings = $mailingService->getDoiMailingKey($id);
                                    if($doiMailings->isSuccess()){
                                        $mailingNames = $mailingService->getName((string) $id);
                                         $doiKey = \simplexml_load_string($doiMailings->getResult());
                                         $doiMailingArray = array(
                                           "doiKey" => (string) $doiKey,
                                           "name" => (string) $mailingNames->getResult()
                                           );
                                     array_push($doiMailingsArray, $doiMailingArray);                                   
                                    }
                                }
			}                       
            return $doiMailingsArray;
        } catch (\Exception $e) {
            throw new MailInOne_WooCommerce_Error($e);
        }
    }
    
    
    protected function contactEvents() {
        try {
            $transactionsService = new com_maileon_api_transactions_TransactionsService($this->getConfing());
            $debug = false;
            $transactionsService->setDebug($debug);
            $contactEvents = $transactionsService->getTransactionTypes();
            $contactEventsArray = array();
            
            foreach ($contactEvents->getResult() as $contactEvent) {
                $contactEventsArray[(string)$contactEvent->id] =  (string)$contactEvent->name;
            }
                  
            return $contactEventsArray;
        } catch (\Exception $e) {
            throw new MailInOne_WooCommerce_Error($e);
        }
    }
        protected function customersFiled() {
        try {
            $result = '';
            $contactsService = new com_maileon_api_contacts_ContactsService($this->getConfing());
            $debug = false;
	    $contactsService->setDebug($debug);
           
            $response = $contactsService->getCustomFields();
            if ($response->isSuccess()) {    
		$result =  $response->getResult();
	}
            return $result;
        } catch (\Exception $e) {
            throw new MailInOne_WooCommerce_Error($e);
        }
    }

    /**
     *
     * @param array $data
     * @return bool
     * @throws MailInOne_WooCommerce_Error
     */
    protected function checkForErrors(array $data) {
        // if we have an array of error data push it into a message
        if (isset($data['errors'])) {
            $message = '';
            foreach ($data['errors'] as $error) {
                $message .= '<p>' . $error['field'] . ': ' . $error['message'] . '</p>';
            }
            throw new MailInOne_WooCommerce_Error($message, $data['status']);
        }

        // make sure the response is correct from the data in the response array
        if (isset($data['status']) && $data['status'] >= 400) {
            throw new MailInOne_WooCommerce_Error($data['detail'], $data['status']);
        }

        return false;
    }

}
