<?php
/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    MailInOne_Woocommerce
 * @subpackage MailInOne_Woocommerce/includes
 * @author     Sami Jarmoud <sami.jarmoud@treaction.de>
 */
class MailInOne_Woocommerce_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		// only do this if the option has never been set before.
		/*if (get_option('mail_in_one_woocommerce_plugin_do_activation_redirect', null) === null) {
			add_option('mail_in_one_woocommerce_plugin_do_activation_redirect', true);
		}
      */
		// create the queue tables because we need them for the sync jobs.
		static::create_queue_tables();

		// update the settings so we have them for use.
		update_option('mail-in-one-woocommerce', array());
	}

	/**
	 * Create the queue tables in the DB so we can use it for syncing.
	 */
	public static function create_queue_tables()
	{
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		global $wpdb;

		$wpdb->hide_errors();

		$charset_collate = $wpdb->get_charset_collate();

                $sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}mio_subscriber (
                                id INT (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
				salutation VARCHAR (255) DEFAULT NULL,
                                first_name VARCHAR (255) DEFAULT NULL,
                                last_name VARCHAR (255) DEFAULT NULL,
				email VARCHAR (100) DEFAULT NULL,
                                street_address VARCHAR (255) DEFAULT NULL,
                                postal_code VARCHAR (255) DEFAULT NULL,
                                city VARCHAR (255) DEFAULT NULL,
                                country VARCHAR (255) DEFAULT NULL,
                                telephone VARCHAR (255) DEFAULT NULL,
                                organization VARCHAR (255) DEFAULT NULL,
                                role VARCHAR (255) DEFAULT NULL,
                                status  VARCHAR (255) DEFAULT NULL,
                                user_registered datetime DEFAULT NULL,
                                created_at datetime DEFAULT NULL
				) $charset_collate;";

		dbDelta( $sql );
                
                 $sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}mio_short_form (
                                id INT (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
				short_code VARCHAR (255) DEFAULT NULL,
                                name VARCHAR (255) DEFAULT NULL,
                                typeOfIntegration VARCHAR (255) DEFAULT NULL,
                                contactEvent_name VARCHAR (255) DEFAULT NULL,
                                contactEvent_key VARCHAR (255) DEFAULT NULL,
                                doi_name VARCHAR (255) DEFAULT NULL,
                                doi_key VARCHAR (255) DEFAULT NULL,
                                marketing_automation INT (11) DEFAULT NULL,
                                status VARCHAR (255) DEFAULT NULL                
				) $charset_collate;";

		dbDelta( $sql );
		update_site_option('mail_in_one_woocommerce_version', mail_in_one_environment_variables()->version);
	}
}
